package com.app;

import android.app.Application;
import android.content.Context;

import androidx.multidex.MultiDex;



public class MyApplication extends Application {
    private static MyApplication sInstance;
    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;

    }
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
    public static MyApplication getInstance() {
        return MyApplication.sInstance;
    }



}
