package com.app.hotfreshdelivery.Modules;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

public class ModelDeleveryFB {

    private String userId ;
    private String userName ;
    private String DeliveryId ;
    private String DeliveryName ;
    private String DeliveryPic ;
    private String DeliveryMobile ;
    private String OrderId ;
    private String DeliveryLate ;
    private String DeliveryLang ;
    private String DrationAndDistance ;
    private String Dration ;
    private String Distance ;
    private String Status ;
    private List<LatLongHistory> TrackingList = null ;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getDeliveryId() {
        return DeliveryId;
    }

    public void setDeliveryId(String deliveryId) {
        DeliveryId = deliveryId;
    }

    public String getDeliveryName() {
        return DeliveryName;
    }

    public void setDeliveryName(String deliveryName) {
        DeliveryName = deliveryName;
    }

    public String getDeliveryPic() {
        return DeliveryPic;
    }

    public void setDeliveryPic(String deliveryPic) {
        DeliveryPic = deliveryPic;
    }

    public String getOrderId() {
        return OrderId;
    }

    public void setOrderId(String orderId) {
        OrderId = orderId;
    }

    public String getDeliveryLate() {
        return DeliveryLate;
    }

    public void setDeliveryLate(String deliveryLate) {
        DeliveryLate = deliveryLate;
    }

    public String getDeliveryLang() {
        return DeliveryLang;
    }

    public void setDeliveryLang(String deliveryLang) {
        DeliveryLang = deliveryLang;
    }

    public String getDeliveryMobile() {
        return DeliveryMobile;
    }

    public void setDeliveryMobile(String deliveryMobile) {
        DeliveryMobile = deliveryMobile;
    }

    public String getDrationAndDistance() {
        return DrationAndDistance;
    }

    public void setDrationAndDistance(String drationAndDistance) {
        DrationAndDistance = drationAndDistance;
    }

    public String getDration() {
        return Dration;
    }

    public void setDration(String dration) {
        Dration = dration;
    }

    public String getDistance() {
        return Distance;
    }

    public void setDistance(String distance) {
        Distance = distance;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public List<LatLongHistory> getTrackingList() {
        return TrackingList;
    }

    public void setTrackingList(List<LatLongHistory> trackingList) {
        TrackingList = trackingList;
    }
}
