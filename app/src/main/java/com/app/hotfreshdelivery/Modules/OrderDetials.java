package com.app.hotfreshdelivery.Modules;

import com.google.gson.annotations.SerializedName;

public class OrderDetials {
    private String BillDate;
    private String BillTime;
    private String BillCode;
    private String CustName;
    private String CustCode;
    private String CustMobile;
    private String Total;
    private String Tax;
    private String CustAddress;
    private String Lat;
    @SerializedName("Long")
    private String aLong;

    private String OrderStatus;

    private String PaymentType;
    private String DeliveryType;
    private String ReceivedDate;
    private String ReceivedTime;
    private String Notes;
    private String CharitiesName;
    private String CheckID;
    private String Depositamount;
    @SerializedName("Status")
    private String status;
    @SerializedName("Message")
    private String Message;
    private String Company;

    public OrderDetials() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getBillDate() {
        return BillDate;
    }

    public String getBillTime() {
        return BillTime;
    }

    public String getBillCode() {
        return BillCode;
    }

    public String getCustName() {
        return CustName;
    }

    public String getCustCode() {
        return CustCode;
    }

    public String getCustMobile() {
        return CustMobile;
    }

    public String getTotal() {
        return Total;
    }

    public String getTax() {
        return Tax;
    }

    public String getCustAddress() {
        return CustAddress;
    }

    public String getLat() {
        return Lat;
    }

    public String getaLong() {
        return aLong;
    }

    public String getPaymentType() {
        return PaymentType;
    }

    public String getReceivedDate() {
        return ReceivedDate;
    }

    public String getReceivedTime() {
        return ReceivedTime;
    }

    public String getNotes() {
        return Notes;
    }

    public String getCharitiesName() {
        return CharitiesName;
    }

    public String getCheckID() {
        return CheckID;
    }

    public String getDepositamount() {
        return Depositamount;
    }

    public String getDeliveryType() {
        return DeliveryType;
    }

    public String getOrderStatus() {
        return OrderStatus;
    }

    public String getCompany() {
        return Company;
    }

    public void setCompany(String company) {
        Company = company;
    }
}
