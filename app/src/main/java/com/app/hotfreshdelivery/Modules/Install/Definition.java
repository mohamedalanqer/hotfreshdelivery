package com.app.hotfreshdelivery.Modules.Install;

public class Definition {

    private String FaceBook ;
    private String Instgram ;
    private String Twitter ;
    private String CloseAppStatus ;
    private String CloseMsg ;
    private String Privacy ;
    private String AboutUs ;
    private String Tax ;

    public Definition() {
    }

    public String getFaceBook() {
        return FaceBook;
    }

    public void setFaceBook(String faceBook) {
        FaceBook = faceBook;
    }

    public String getInstgram() {
        return Instgram;
    }

    public void setInstgram(String instgram) {
        Instgram = instgram;
    }

    public String getTwitter() {
        return Twitter;
    }

    public void setTwitter(String twitter) {
        Twitter = twitter;
    }

    public String getCloseAppStatus() {
        return CloseAppStatus;
    }

    public void setCloseAppStatus(String closeAppStatus) {
        CloseAppStatus = closeAppStatus;
    }

    public String getCloseMsg() {
        return CloseMsg;
    }

    public void setCloseMsg(String closeMsg) {
        CloseMsg = closeMsg;
    }

    public String getPrivacy() {
        return Privacy;
    }

    public void setPrivacy(String privacy) {
        Privacy = privacy;
    }

    public String getAboutUs() {
        return AboutUs;
    }

    public void setAboutUs(String aboutUs) {
        AboutUs = aboutUs;
    }

    public String getTax() {
        return Tax;
    }

    public void setTax(String tax) {
        Tax = tax;
    }
}
