package com.app.hotfreshdelivery.Modules.Map;

public class Legs {

    private Distance distance ;
    private Duration duration ;

    public Distance getDistance() {
        return distance;
    }

    public void setDistance(Distance distance) {
        this.distance = distance;
    }

    public Duration getDuration() {
        return duration;
    }

    public void setDuration(Duration duration) {
        this.duration = duration;
    }
}
