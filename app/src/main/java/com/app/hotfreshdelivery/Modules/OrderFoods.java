package com.app.hotfreshdelivery.Modules;

public class OrderFoods {

    private int order_id ;
    private int food_id ;
    private int quantity ;
    private double price ;
    private int total_cost ;
    private boolean is_complete ;
    private String note ;
    //private  FoodData  food_data ;

    public int getOrder_id() {
        return order_id;
    }

    public void setOrder_id(int order_id) {
        this.order_id = order_id;
    }

    public int getFood_id() {
        return food_id;
    }

    public void setFood_id(int food_id) {
        this.food_id = food_id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getTotal_cost() {
        return total_cost;
    }

    public void setTotal_cost(int total_cost) {
        this.total_cost = total_cost;
    }

    public boolean isIs_complete() {
        return is_complete;
    }

    public void setIs_complete(boolean is_complete) {
        this.is_complete = is_complete;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

 /**   public FoodData getFood_data() {
        return food_data;
    }

    public void setFood_data(FoodData food_data) {
        this.food_data = food_data;
    }*/
}
