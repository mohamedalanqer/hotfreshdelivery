package com.app.hotfreshdelivery.Modules;

public class Order {
    private String BillCode ;
    private String ClientName ;
    private String ClientAddress ;
    private String BillTotal ;
    private String ClientMobile ;


    public Order() {
    }

    public String getBillCode() {
        return BillCode;
    }

    public void setBillCode(String billCode) {
        BillCode = billCode;
    }

    public String getClientName() {
        return ClientName;
    }

    public void setClientName(String clientName) {
        ClientName = clientName;
    }

    public String getClientAddress() {
        return ClientAddress;
    }

    public void setClientAddress(String clientAddress) {
        ClientAddress = clientAddress;
    }

    public String getBillTotal() {
        return BillTotal;
    }

    public void setBillTotal(String billTotal) {
        BillTotal = billTotal;
    }

    public String getClientMobile() {
        return ClientMobile;
    }

    public void setClientMobile(String clientMobile) {
        ClientMobile = clientMobile;
    }
}
