package com.app.hotfreshdelivery.Modules;


public class ItemService {

    private int IdItem;

    private String ServiceNameAra;
    private String ServiceNameEng;
    private String ServicePrice;
    private String ServiceCode;
    private String ItemCode;

    private boolean isIs_selected;

    public ItemService() {
    }

    public String getServiceNameAra() {
        return ServiceNameAra;
    }

    public void setServiceNameAra(String serviceNameAra) {
        ServiceNameAra = serviceNameAra;
    }

    public String getServiceNameEng() {
        return ServiceNameEng;
    }

    public void setServiceNameEng(String serviceNameEng) {
        ServiceNameEng = serviceNameEng;
    }

    public String getServicePrice() {
        return ServicePrice;
    }

    public void setServicePrice(String servicePrice) {
        ServicePrice = servicePrice;
    }

    public String getServiceCode() {
        return ServiceCode;
    }

    public void setServiceCode(String serviceCode) {
        ServiceCode = serviceCode;
    }

    public boolean isIs_selected() {
        return isIs_selected;
    }

    public void setIs_selected(boolean is_selected) {
        isIs_selected = is_selected;
    }

    public int getIdItem() {
        return IdItem;
    }

    public void setIdItem(int idItem) {
        IdItem = idItem;
    }

    public String getItemCode() {
        return ItemCode;
    }

    public void setItemCode(String itemCode) {
        ItemCode = itemCode;
    }

    @Override
    public String toString() {
        return "ItemService{" +
                "IdItem=" + IdItem +
                ", ServiceNameAra='" + ServiceNameAra + '\'' +
                ", ServiceNameEng='" + ServiceNameEng + '\'' +
                ", ServicePrice='" + ServicePrice + '\'' +
                ", ServiceCode='" + ServiceCode + '\'' +
                ", ItemCode='" + ItemCode + '\'' +
                ", isIs_selected=" + isIs_selected +
                '}';
    }
}
