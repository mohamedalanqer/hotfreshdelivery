package com.app.hotfreshdelivery.Modules.Map;

public class DurationValue {
    String distance ;
    String duration ;

    public DurationValue() {
    }

    public DurationValue(String distance, String duration) {
        this.distance = distance;
        this.duration = duration;
    }

    public String getDistance() {
        return distance;
    }

    public String getDuration() {
        return duration;
    }
}
