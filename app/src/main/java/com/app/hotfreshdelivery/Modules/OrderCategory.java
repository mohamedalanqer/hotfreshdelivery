package com.app.hotfreshdelivery.Modules;

public class OrderCategory {
    private int CategoryID ;
    private String TagName ;
    private String CategoryName ;
    private boolean IsSelect = false  ;

    public OrderCategory() {
    }

    public String getTagName() {
        return TagName;
    }

    public OrderCategory(int categoryID, String tagName, String categoryName) {
        CategoryID = categoryID;
        TagName = tagName;
        CategoryName = categoryName;
    }

    public void setTagName(String tagName) {
        TagName = tagName;
    }

    public OrderCategory(int categoryID, String categoryName) {
        CategoryID = categoryID;
        CategoryName = categoryName;
    }

    public int getCategoryID() {
        return CategoryID;
    }

    public void setCategoryID(int categoryID) {
        CategoryID = categoryID;
    }

    public String getCategoryName() {
        return CategoryName;
    }

    public void setCategoryName(String categoryName) {
        CategoryName = categoryName;
    }

    public boolean isSelect() {
        return IsSelect;
    }

    public void setSelect(boolean select) {
        IsSelect = select;
    }
}
