package com.app.hotfreshdelivery.directionhelpers;

public interface  TaskLoadedCallback {
    void onTaskDone(Object... values);
}
