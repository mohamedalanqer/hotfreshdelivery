package com.app.hotfreshdelivery.Ui.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;


import com.app.hotfreshdelivery.Callback.OnItemClickTagListener;
import com.app.hotfreshdelivery.Manager.FontManager;
import com.app.hotfreshdelivery.Modules.Order;
import com.app.hotfreshdelivery.R;

import org.json.JSONException;

import java.io.FileNotFoundException;
import java.util.List;


public class RecyclerOrders extends RecyclerView.Adapter<RecyclerOrders.CustomView> {
    Context context;
    List<Order> mylist;
    private OnItemClickTagListener listener;
    static public int positionSelected = -1;
    int Layout;

    public class CustomView extends RecyclerView.ViewHolder {
        TextView text_name_user, text_address, text_price;
        Button btn_action;


        public CustomView(View view) {
            super(view);
            text_name_user = itemView.findViewById(R.id.text_name_user);
            text_address = itemView.findViewById(R.id.text_address);
            text_price = itemView.findViewById(R.id.text_price);
            btn_action = itemView.findViewById(R.id.btn_action);
            FontManager.applyFont(context, view);


        }
    }

    public RecyclerOrders(Context context, List<Order> mylist, int Layout, int positionSelected, OnItemClickTagListener listener) {
        this.context = context;
        this.mylist = mylist;
        this.listener = listener;
        this.positionSelected = positionSelected;
        this.Layout = Layout;
    }

    @Override
    public CustomView onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(Layout, parent, false);
        CustomView viewholder = new CustomView(view);
        return viewholder;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(final CustomView holder, final int position) {
        Order order = mylist.get(position);
        holder.text_name_user.setText(order.getClientName() + "");
        if (order.getClientAddress() != null) {
            holder.text_address.setText(order.getClientAddress() + "");
        }else {
            holder.text_address.setText( ""+context.getResources().getString(R.string.undefined));
        }
        holder.text_price.setText(order.getBillTotal() + " " + context.getResources().getString(R.string.SAR));


        holder.btn_action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    listener.onItemClick(v, position, "view");
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        });

    }
    public void filterList(List<Order> orderList) {
        this.mylist = orderList;
        notifyDataSetChanged();
    }
    @Override
    public int getItemCount() {
        return mylist.size();
    }


}

