package com.app.hotfreshdelivery.Ui.Activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.app.hotfreshdelivery.Callback.OnItemClickTagListener;
import com.app.hotfreshdelivery.Manager.AppLanguage;
import com.app.hotfreshdelivery.Manager.AppPreferences;
import com.app.hotfreshdelivery.Manager.FontManager;
import com.app.hotfreshdelivery.Modules.Install.Branch;
import com.app.hotfreshdelivery.Modules.User;
import com.app.hotfreshdelivery.R;
import com.app.hotfreshdelivery.Ui.Adapters.RecyclerBranch;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public class InfoActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {




    private TextView toolbarNameTxt;
    private RelativeLayout Layout_notification;
    private RelativeLayout end_main;
    private TextView info_text;
    SwipeRefreshLayout swipeRefreshLayout;
    private ImageView image_toolbar_start ;


    private Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppLanguage.setContentLang(this);

        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        setContentView(R.layout.activity_info);
        FontManager.applyFont(this, findViewById(R.id.layout));
        handler = new Handler(Looper.getMainLooper());


        initSetup();

    }


    User user = new User();
    List<Branch> branchList = new ArrayList<>();
     private String TagAction = "";
    RecyclerBranch adapter_branch;
    RecyclerView recycler_view;
    public User getUserDetials() {
        Gson gson = new Gson();
        String json = AppPreferences.getString(InfoActivity.this, "userJson");
        if (json != null) {
            if (!TextUtils.equals("0", json)) {
                return gson.fromJson(json, User.class);
            }

        }
        return null;
    }
    private void initSetup() {
        toolbarNameTxt = findViewById(R.id.toolbarNameTxt);
        Layout_notification = findViewById(R.id.Layout_notification);
        info_text = findViewById(R.id.info_text);
        recycler_view = findViewById(R.id.recycler_view);
        image_toolbar_start = findViewById(R.id.image_toolbar_start);
        image_toolbar_start.setVisibility(View.VISIBLE);
        image_toolbar_start.setImageResource(R.drawable.ic_baseline_arrow_back_ios_24);

        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(this);

        end_main = findViewById(R.id.end_main);
        end_main.setOnClickListener(view -> {
            onBackPressed();
        });
        user = getUserDetials();


        Layout_notification.setVisibility(View.GONE);

        String branchesJson = AppPreferences.getString(InfoActivity.this, "branchesJson");
        String definitionsJson = AppPreferences.getString(InfoActivity.this, "definitionsJson");
        if (branchList.size() < 1) {
            if (branchesJson != null) {
                branchList = new Gson().fromJson(branchesJson, new TypeToken<List<Branch>>() {
                }.getType());
            }
        }


            toolbarNameTxt.setText("" + getResources().getString(R.string.Branchs));
            recycler_view.setVisibility(View.VISIBLE);
            info_text.setVisibility(View.GONE);
            adapter_branch = new RecyclerBranch(this, branchList, 10000, R.layout.row_item_branch_info, new OnItemClickTagListener() {
                @Override
                public void onItemClick(View view, int position, String tag) throws JSONException, FileNotFoundException {
                    if (TextUtils.equals("map", tag)) {
                        GoToMapBranchActivity(branchList.get(position).getLat(), branchList.get(position).getLong());
                    }

                }
            });
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
            recycler_view.setLayoutManager(linearLayoutManager);
            recycler_view.setAdapter(adapter_branch);
            adapter_branch.notifyDataSetChanged();



    }

    private void GoToMapBranchActivity(String lat, String aLong) {
        Uri gmmIntentUri = Uri.parse("google.streetview:cbll=" + lat + "," + aLong);
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        startActivity(mapIntent);
    }


    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(false);
                initSetup();
            }
        }, 2000);
    }

}
