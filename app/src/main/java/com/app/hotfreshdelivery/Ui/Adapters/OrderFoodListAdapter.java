package com.app.hotfreshdelivery.Ui.Adapters;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;


import com.app.hotfreshdelivery.Callback.OnItemClickTagListener;
import com.app.hotfreshdelivery.Modules.OnlineOrder;
import com.app.hotfreshdelivery.Modules.OrderFoods;
import com.app.hotfreshdelivery.R;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import org.json.JSONException;

import java.io.FileNotFoundException;
import java.util.List;

public class OrderFoodListAdapter extends RecyclerView.Adapter<OrderFoodListAdapter.MyViewHolder> {
    private Context context;
    private List<OnlineOrder> mylist;
    public static int SelectItem = -1;
    private OnItemClickTagListener listener;

    public OrderFoodListAdapter(Context context, List<OnlineOrder> arrayList, int SelectItem, OnItemClickTagListener listener) {
        this.context = context;
        this.mylist = arrayList;
        this.SelectItem = SelectItem;
        this.listener = listener;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView row_img_item;
        private TextView text_service, text_name;

        public MyViewHolder(View view) {
            super(view);
            row_img_item = view.findViewById(R.id.row_img_item);
            text_service = view.findViewById(R.id.text_service);
            text_name = view.findViewById(R.id.text_name);

        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.orderfood_item_view, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        Log.e("adatper","MyListDate" + mylist.get(position).getItemName() ) ;
        holder.text_name.setText(mylist.get(position).getItemName() + "");
        holder.text_service.setVisibility(View.GONE);
        if (mylist.get(position).getItemServices() != null) {
            if (mylist.get(position).getItemServices().size() > 0)
                holder.text_service.setVisibility(View.VISIBLE);
        }

        holder.text_service.setOnClickListener(v -> {
            try {
                listener.onItemClick(v, position, "service");
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

        });


        if (mylist.get(position).getImage() != null) {
            Log.e("adatper", mylist.get(position).getImage() +" image");
            Glide.with(context).load(mylist.get(position).getImage()).into(holder.row_img_item);
        }
    }


    @Override
    public int getItemCount() {
        return mylist.size();
    }


}