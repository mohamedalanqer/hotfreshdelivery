package com.app.hotfreshdelivery.Ui.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.app.hotfreshdelivery.Manager.AppErrorsManager;
import com.app.hotfreshdelivery.Manager.AppLanguage;
import com.app.hotfreshdelivery.Manager.AppPreferences;
import com.app.hotfreshdelivery.Manager.FontManager;
import com.app.hotfreshdelivery.Manager.RootManager;
import com.app.hotfreshdelivery.Modules.User;
import com.app.hotfreshdelivery.R;
import com.app.hotfreshdelivery.webService.RetrofitWebService;
import com.app.hotfreshdelivery.webService.model.response.LoginResponse;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;
import com.tuyenmonkey.mkloader.MKLoader;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    MKLoader mkLoader;
    Button btn_login;
    EditText emailEditText, passwordEditText;
    String deviceToken = "";

    public User getUserDetails() {
        User user = new User();
        Gson gson = new Gson();
        String json = AppPreferences.getString(this, "userJson");
        if (json != null) {
            if (!TextUtils.equals("0", json)) {
                return gson.fromJson(json, User.class);
            }

        }
        return null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppLanguage.setContentLang(this);
        setContentView(R.layout.activity_login);
        View layout = findViewById(R.id.layout);
        FontManager.applyFont(this, layout);
        //     printKeyHash();
        if (getUserDetails() != null) {
            Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
            startActivity(intent);
            finish();
        }
        initSetUp();
    }

    private void initSetUp() {

        emailEditText = findViewById(R.id.emailEditText);
        passwordEditText = findViewById(R.id.passwordEditText);
        btn_login = findViewById(R.id.btn_login);

        btn_login.setOnClickListener(this);
        mkLoader = findViewById(R.id.mkLoader);

        FirebaseApp.initializeApp(this);
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(LoginActivity.this, new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                //     newToken = instanceIdResult.getToken();
                Log.e("newToken", deviceToken);

            }
        });


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btn_login:
                CreateLoginWebService();
                break;


        }
    }

    private void CreateLoginWebService() {

        String mobile = emailEditText.getText().toString().trim();
        String password = passwordEditText.getText().toString().trim();
        if (TextUtils.isEmpty(mobile)) {
            emailEditText.setError(getString(R.string.required_field));
            emailEditText.requestFocus();
        } else if (TextUtils.isEmpty(password)) {
            passwordEditText.setError(getString(R.string.required_field));
            passwordEditText.requestFocus();
        } else {
            mkLoader.setVisibility(View.VISIBLE);
            Log.e("data", deviceToken + " ");
            if (TextUtils.isEmpty(deviceToken)) {
                FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(LoginActivity.this, new OnSuccessListener<InstanceIdResult>() {
                    @Override
                    public void onSuccess(InstanceIdResult instanceIdResult) {
                        //     newToken = instanceIdResult.getToken();
                        Log.e("newToken", deviceToken);

                    }
                });
            }
            RetrofitWebService.getService(this).createLogin(mobile, password, deviceToken, "android").enqueue(new Callback<LoginResponse>() {
                @Override
                public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                    Log.e("createLogin", response.toString());
                    if (RootManager.RESPONSE_CODE_OK == response.code()) {
                        Log.e("createLogin", response.body().getStatus());

                        if (TextUtils.equals("True", response.body().getStatus())) {
                            if (response.body() != null) {
                                User user = (User) response.body().getUser();
                                if (user != null) {
                                    Gson json = new Gson();
                                    String userJson = json.toJson(user);
                                    AppPreferences.saveString(LoginActivity.this, "userJson", userJson);
                                    if (!userJson.isEmpty()) {

                                        Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(intent);
                                        finish();
                                    }


                                } else {
                                    Toast.makeText(LoginActivity.this, "" + false, Toast.LENGTH_SHORT).show();
                                    AppErrorsManager.showCustomErrorDialog(LoginActivity.this, getResources().getString(R.string.error),
                                            response.body().getMessage());
                                }
                            } else {
                                AppErrorsManager.showCustomErrorDialog(LoginActivity.this, getResources().getString(R.string.error),
                                        response.body().getMessage());
                            }
                        } else if (TextUtils.equals("False", response.body().getStatus())) {
                            AppErrorsManager.showCustomErrorDialog(LoginActivity.this, getResources().getString(R.string.error),
                                    response.body().getMessage());
                        }
                    } else {
                        AppErrorsManager.showCustomErrorDialog(LoginActivity.this, getResources().getString(R.string.Failure),
                                getResources().getString(R.string.InternalServerError));
                    }
                    mkLoader.setVisibility(View.GONE);
                }

                @Override
                public void onFailure(Call<LoginResponse> call, Throwable t) {
                    Log.e("createLogin", t.toString());
                    mkLoader.setVisibility(View.GONE);
                    AppErrorsManager.showCustomErrorDialog(LoginActivity.this, getResources().getString(R.string.Failure),
                            t.getMessage() + "");
                }
            });

        }


    }
}
