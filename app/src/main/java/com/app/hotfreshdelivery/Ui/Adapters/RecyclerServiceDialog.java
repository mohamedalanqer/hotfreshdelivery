package com.app.hotfreshdelivery.Ui.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;


import com.app.hotfreshdelivery.Callback.OnItemClickTagListener;
import com.app.hotfreshdelivery.Manager.FontManager;
import com.app.hotfreshdelivery.Modules.ItemService;
import com.app.hotfreshdelivery.R;

import org.json.JSONException;

import java.io.FileNotFoundException;
import java.util.List;


public class RecyclerServiceDialog extends RecyclerView.Adapter<RecyclerServiceDialog.CustomView> {
    Context context;
    List<ItemService> mylist;
    private OnItemClickTagListener listener;
    int Layout;


    public class CustomView extends RecyclerView.ViewHolder {
        TextView row_name_service;
        ImageView row_img_service_check;

        public CustomView(View v) {
            super(v);
            FontManager.applyFont(context, v);
            row_name_service = v.findViewById(R.id.row_name_service);
            row_img_service_check = v.findViewById(R.id.row_img_service_check);

        }

    }

    public RecyclerServiceDialog() {
    }

    public RecyclerServiceDialog(Context context, List<ItemService> mylist, int Layout, OnItemClickTagListener listener) {
        this.context = context;
        this.mylist = mylist;
        this.listener = listener;
        this.Layout = Layout;
    }

    @Override
    public CustomView onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(Layout, parent, false);
        CustomView viewholder = new CustomView(view);
        return viewholder;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(final CustomView holder, final int position) {
        ItemService itemService = mylist.get(position);
        holder.row_name_service.setText(itemService.getServiceNameAra() + " : " + itemService.getServicePrice() + " "
                + context.getResources().getString(R.string.SAR));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    listener.onItemClick(view, position, "select");
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        });

        holder.row_img_service_check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    listener.onItemClick(view, position, "select");
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        });
    }




    @Override
    public int getItemCount() {
        return mylist.size();
    }


}

