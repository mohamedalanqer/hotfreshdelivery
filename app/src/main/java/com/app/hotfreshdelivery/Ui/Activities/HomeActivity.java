package com.app.hotfreshdelivery.Ui.Activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.hotfreshdelivery.Callback.InstallCallback;
import com.app.hotfreshdelivery.Callback.OnItemClickTagListener;
import com.app.hotfreshdelivery.Manager.AppErrorsManager;
import com.app.hotfreshdelivery.Manager.AppLanguage;
import com.app.hotfreshdelivery.Manager.AppMp3Manager;
import com.app.hotfreshdelivery.Manager.AppPreferences;
import com.app.hotfreshdelivery.Manager.FontManager;
import com.app.hotfreshdelivery.Manager.RootManager;
import com.app.hotfreshdelivery.Modules.Order;
import com.app.hotfreshdelivery.Modules.User;
import com.app.hotfreshdelivery.R;
import com.app.hotfreshdelivery.Ui.Adapters.RecyclerOrders;
import com.app.hotfreshdelivery.webService.RetrofitWebService;
import com.app.hotfreshdelivery.webService.model.response.OrdersResponse;
import com.bumptech.glide.Glide;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.navigation.NavigationView;
import com.google.gson.Gson;
import com.tuyenmonkey.mkloader.MKLoader;

import org.json.JSONException;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener, NavigationView.OnNavigationItemSelectedListener, SwipeRefreshLayout.OnRefreshListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    Double latitude = 0.0, longitude = 0.0;
    static final int REQUST_CODE = 1000;
    RecyclerView recyclerview;
    RecyclerOrders adapter;
    MKLoader mkLoader;
    List<Order> orderList = new ArrayList<>();
    LinearLayout layout_empty;
    SwipeRefreshLayout swipeRefreshLayout;
    LinearLayoutManager linearLayoutManager;
    //  ActionBar toolbar;
    TextView toolbarNameTxt;
    private EditText searchEditText;
    private FusedLocationProviderClient fusedLocationClient;


    private TextView text_action_go, text_close_dialog;
    private LinearLayout layout_screenLock;
    private AppMp3Manager appMp3Manager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppLanguage.setContentLang(this);
        setContentView(R.layout.activity_home);
        View layout = findViewById(R.id.layout);
        toolbarNameTxt = findViewById(R.id.toolbarNameTxt);

        FontManager.applyFont(this, layout);
        initSetUp();


    }

    private void initSetUp() {
        initSetupDrawer();
        recyclerview = findViewById(R.id.recyclerview);
        layout_empty = findViewById(R.id.layout_empty);
        mkLoader = findViewById(R.id.mkLoader);
        searchEditText = findViewById(R.id.searchEditText);


        text_action_go = findViewById(R.id.text_action_go);
        text_close_dialog = findViewById(R.id.text_close_dialog);
        layout_screenLock = findViewById(R.id.layout_screenLock);
        text_action_go.setOnClickListener(this::onClick);
        text_close_dialog.setOnClickListener(this::onClick);

        appMp3Manager = new AppMp3Manager(this);


        searchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String search = charSequence.toString().trim();
                if (TextUtils.isEmpty(search)) {

                } else {
                    filter(search);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(this);

        PackageManager pm2 = getPackageManager();
        int hasPerm4 = pm2.checkPermission(Manifest.permission.ACCESS_FINE_LOCATION, getPackageName());
        if (hasPerm4 == PackageManager.PERMISSION_GRANTED) {
            ChickIsGPSEnable();
        } else {
            checkAndRequestPermissionsLocation();
        }


        CheckIsBatteryOptimization();
    }

    private void CheckIsBatteryOptimization() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Intent intentPower = new Intent();
            String packageName = getPackageName();
            PowerManager pm = (PowerManager) getSystemService(POWER_SERVICE);
            if (!pm.isIgnoringBatteryOptimizations(packageName)) {
                /**  intentPower.setAction(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
                 intentPower.setData(Uri.parse("package:" + packageName));
                 startActivity(intentPower);*/
                Animation slideUp = AnimationUtils.loadAnimation(this, R.anim.slide_up);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        layout_screenLock.setVisibility(View.VISIBLE);
                        layout_screenLock.startAnimation(slideUp);
                        appMp3Manager.palyNotificationMusic(R.raw.bell_sound);
                    }
                }, 1000);

            } else {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        layout_screenLock.setVisibility(View.GONE);
                    }
                }, 1000);
            }
        }
    }


    DrawerLayout drawer;
    ActionBarDrawerToggle toggle;

    private void initSetupDrawer() {
        MaterialToolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);


        toggle = new ActionBarDrawerToggle(
                this,
                drawer,
                toolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);

        toggle.setDrawerIndicatorEnabled(false);


        toggle.setToolbarNavigationClickListener(v -> {
            if (drawer.isDrawerVisible(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            } else {
                drawer.openDrawer(GravityCompat.START);

            }
        });

        int resMenu = R.drawable.ic_menu;
        String lang = AppLanguage.getLanguage(this);
        if (TextUtils.equals("ar", lang)) {
            resMenu = R.drawable.ic_menu;
        } else {
            resMenu = R.drawable.ic_menu;
        }
        drawer.addDrawerListener(toggle);
        Drawable drawable = ResourcesCompat.getDrawable(getResources(), resMenu, this.getTheme());
        toggle.setHomeAsUpIndicator(drawable);

        toggle.syncState();


        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setItemIconTintList(null);
        View hView = navigationView.getHeaderView(0);
        TextView nav_user = (TextView) hView.findViewById(R.id.nav_name);
        ImageView nav_image = hView.findViewById(R.id.nav_image);
        if (getUserDetials() != null) {
            nav_user.setText(getUserDetials().getName());
            Glide.with(this).load(getUserDetials().getImage()).error(R.drawable.ic_logo).into(nav_image);
        }
        FontManager.ChangeFontMenu(getApplicationContext(), navigationView);


    }

    private List<Order> ordersfilter = new ArrayList<>();

    private void openPowerSettings() {
        /**  Intent intent = new Intent();
         intent.setData(Uri.parse("package:" + getApplicationContext().getPackageName()));
         intent.setAction(Settings.ACTION_IGNORE_BATTERY_OPTIMIZATION_SETTINGS);
         startActivity(intent);*/
        startActivityForResult(new Intent(android.provider.Settings.ACTION_IGNORE_BATTERY_OPTIMIZATION_SETTINGS), 0);

    }

    private void filter(String text) {
        if (orderList.size() > 0) {
            //new array list that will hold the filtered data
            ordersfilter = new ArrayList<>();

            //looping through existing elements
            for (Order s : orderList) {

                if (s.getClientName().toLowerCase().contains(text.toLowerCase())) {
                    ordersfilter.add(s);
                }
            }
            //calling a method of the adapter class and passing the filtered list
            adapter.filterList(ordersfilter);
        }
    }


    public void getLocation() {
        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;

        try {
            assert lm != null;
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ignored) {
        }

        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ignored) {
        }

        if (!gps_enabled && !network_enabled) {
            // notify user
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.setMessage(getResources().getString(R.string.gps_network_not_enabled));
            dialog.setPositiveButton(getResources().getString(R.string.open_location_settings), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    // TODO Auto-generated method stub
                    Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(myIntent);
                    //get gps
                }
            });
            dialog.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    // TODO Auto-generated method stub

                }
            });
            dialog.show();
        } else {
            GetLocationUser();
        }
    }


    public void GetLocationUser() {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        PackageManager pm2 = getPackageManager();
        int hasPerm4 = pm2.checkPermission(Manifest.permission.ACCESS_FINE_LOCATION, getPackageName());
        if (hasPerm4 == PackageManager.PERMISSION_GRANTED) {

            fusedLocationClient.getLastLocation()
                    .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            if (location != null) {
                                latitude = location.getLatitude();
                                longitude = location.getLongitude();
                                GetOnWayOrdersListWebService();

                            } else {
                                GetOnWayOrdersListWebService();
                            }
                        }
                    });


        } else {
            checkAndRequestPermissionsLocation();
        }

    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void startLocationButtonClick(Location location, Order order) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            if (this.checkPermission()) {
                AppErrorsManager.showPermissionLocationBackGroundNotCancel(HomeActivity.this,
                        getResources().getString(R.string.PermissionLocation)
                        , getResources().getString(R.string.PermissionLocationMsg), new InstallCallback() {
                            @Override
                            public void onStatusDone(String status) {
                                if (TextUtils.equals("ok", status)) {
                                    askPermission();
                                }
                            }
                        });
            } else {
                String str_order = new Gson().toJson(order, Order.class);
                Intent intent = new Intent(getApplicationContext(), MapsActivity.class);
                intent.putExtra("str_order", str_order);
                intent.putExtra("BillCode", order.getBillCode());

                if (location != null) {
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();
                    intent.putExtra("Mylatitude", latitude);
                    intent.putExtra("Mylongitude", longitude);
                    startActivityForResult(intent, 201);
                } else {
                    if (latitude != 0.0 && longitude != 0.0) {
                        intent.putExtra("Mylatitude", latitude);
                        intent.putExtra("Mylongitude", longitude);
                        startActivityForResult(intent, 201);
                    } else {
                        Toast.makeText(HomeActivity.this, "لم يتم تحديد عنوانك حتي الأن ! شغل خدمات الموقع وحاول مجددا", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }


    }

    private boolean checkPermission() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_BACKGROUND_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            return false;
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            return ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_BACKGROUND_LOCATION) != PackageManager.PERMISSION_GRANTED;
        }
        return true;
    }

    private void askPermission() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            ActivityCompat.requestPermissions(Objects.requireNonNull(this), new String[]{Manifest.permission.ACCESS_BACKGROUND_LOCATION}, 1);
        }

    }


    GoogleApiClient googleApiClient;

    private void ChickIsGPSEnable() {
        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this).build();
            googleApiClient.connect();
            LocationRequest locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(30 * 1000);
            locationRequest.setFastestInterval(5 * 1000);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(locationRequest);

            //**************************
            builder.setAlwaysShow(true); //this is the key ingredient
            //**************************
            PendingResult<LocationSettingsResult> result =
                    LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(LocationSettingsResult result) {
                    final Status status = result.getStatus();
                    final LocationSettingsStates state = result.getLocationSettingsStates();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.SUCCESS:
                            // All location settings are satisfied. The client can initialize location
                            // requests here.
                            getLocation();
                            break;
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            // Location settings are not satisfied. But could be fixed by showing the user
                            // a dialog.
                            try {
                                // Show the dialog by calling startResolutionForResult(),
                                // and check the result in onActivityResult().
                                status.startResolutionForResult(
                                        HomeActivity.this, 1000);
                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            // Location settings are not satisfied. However, we have no way to fix the
                            // settings so we won't show the dialog.
                            break;
                    }
                }
            });
        }
    }

    private void checkAndRequestPermissionsLocation() {
        String[] permissions = new String[]{
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION
        };
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String permission : permissions) {
            if (ContextCompat.checkSelfPermission(getApplicationContext(), permission) != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(permission);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUST_CODE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    GetLocationUser();
                }
                break;
        }
    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                GetOnWayOrdersListWebService();
                swipeRefreshLayout.setRefreshing(false);
            }
        }, 2000);
    }


    public User getUserDetials() {
        Gson gson = new Gson();
        String json = AppPreferences.getString(HomeActivity.this, "userJson");
        if (json != null) {
            if (!TextUtils.equals("0", json)) {
                return gson.fromJson(json, User.class);
            }

        }
        return null;
    }

    private void GetListMyOrderWebService() {
        if (latitude != 0.0 && longitude != 0.0) {
            recyclerview.setVisibility(View.VISIBLE);
            layout_empty.setVisibility(View.GONE);
            mkLoader.setVisibility(View.VISIBLE);
            User user = getUserDetials();
            if (user != null) {
                toolbarNameTxt.setText("" + getResources().getString(R.string.MyNewOrder));
                RetrofitWebService.getService(this).GetMyOrdersList(user.getAccessToken(), latitude + "", longitude + "").enqueue(new Callback<OrdersResponse>() {
                    @Override
                    public void onResponse(Call<OrdersResponse> call, Response<OrdersResponse> response) {
                        Log.e("GetMyOrdersList", response.toString() + " " + latitude + " " + longitude);
                        if (response.code() == RootManager.RESPONSE_CODE_OK) {
                            String message_str = response.body().getMessage();
                            if ("True".equals(response.body().getStatus())) {

                                orderList = response.body().getOrders();
                                if (orderList.size() > 0) {
                                    orderList = response.body().getOrders();
                                    SetUpRecyclerOrders(orderList);
                                    toolbarNameTxt.setText("" + getResources().getString(R.string.MyNewOrder));
                                    recyclerview.setVisibility(View.VISIBLE);
                                    layout_empty.setVisibility(View.GONE);
                                } else {
                                    recyclerview.setVisibility(View.GONE);
                                    layout_empty.setVisibility(View.VISIBLE);
                                }


                            } else if ("False".equals(response.body().getStatus())) {
                                if (!TextUtils.isEmpty(message_str)) {
                                    if (TextUtils.equals("Incorrect Access Token", message_str.trim())) {
                                        AppErrorsManager.showCustomErrorDialogNotCancel(HomeActivity.this,
                                                getResources().getString(R.string.error), message_str, new InstallCallback() {
                                                    @Override
                                                    public void onStatusDone(String status) {
                                                        Gson json = new Gson();
                                                        AppPreferences.saveString(HomeActivity.this, "userJson", "0");
                                                        Intent intent = new Intent(HomeActivity.this, SplashActivity.class);
                                                        startActivity(intent);
                                                        finish();
                                                    }
                                                });
                                    } else {
                                        AppErrorsManager.showCustomErrorDialog(HomeActivity.this,
                                                getResources().getString(R.string.error), message_str);
                                    }
                                } else {
                                    AppErrorsManager.showCustomErrorDialog(HomeActivity.this,
                                            getResources().getString(R.string.error), message_str);
                                }
                            }

                        } else {
                            AppErrorsManager.showCustomErrorDialog(HomeActivity.this,
                                    getResources().getString(R.string.error), response.errorBody().toString());
                        }


                        mkLoader.setVisibility(View.GONE);

                    }

                    @Override
                    public void onFailure(Call<OrdersResponse> call, Throwable t) {
                        AppErrorsManager.showCustomErrorDialog(HomeActivity.this,
                                getResources().getString(R.string.error), t.getMessage() + "");
                        mkLoader.setVisibility(View.GONE);
                    }
                });
            }

        } else {

            AppErrorsManager.showNullLocationDialog(HomeActivity.this, getResources().getString(R.string.info),
                    getResources().getString(R.string.message_location_zero), getResources().getString(R.string.retry), new InstallCallback() {
                        @Override
                        public void onStatusDone(String status) {
                            if (TextUtils.equals("retry", status)) {
                                fusedLocationClient = LocationServices.getFusedLocationProviderClient(HomeActivity.this);
                                PackageManager pm2 = getPackageManager();
                                int hasPerm4 = pm2.checkPermission(Manifest.permission.ACCESS_FINE_LOCATION, getPackageName());
                                if (hasPerm4 == PackageManager.PERMISSION_GRANTED) {
                                    fusedLocationClient.getLastLocation()
                                            .addOnSuccessListener(HomeActivity.this, new OnSuccessListener<Location>() {
                                                @Override
                                                public void onSuccess(Location location) {
                                                    if (location != null) {
                                                        latitude = location.getLatitude();
                                                        longitude = location.getLongitude();
                                                    }
                                                    GetListMyOrderWebService();

                                                }
                                            });

                                } else {
                                    checkAndRequestPermissionsLocation();
                                }
                            }
                        }
                    });

        }
    }

    private void GetOnWayOrdersListWebService() {
        recyclerview.setVisibility(View.VISIBLE);
        layout_empty.setVisibility(View.GONE);
        mkLoader.setVisibility(View.VISIBLE);
        User user = getUserDetials();
        if (user != null) {
            RetrofitWebService.getService(this).GetOnWayOrdersList(user.getAccessToken()).enqueue(new Callback<OrdersResponse>() {
                @Override
                public void onResponse(Call<OrdersResponse> call, Response<OrdersResponse> response) {
                    Log.e("taaaa", response.toString());
                    if (response.code() == RootManager.RESPONSE_CODE_OK) {
                        String message_str = response.body().getMessage();
                        if ("True".equals(response.body().getStatus())) {
                            orderList = response.body().getOrders();
                            if (orderList.size() > 0) {
                                orderList = response.body().getOrders();
                                SetUpRecyclerOrders(orderList);
                                toolbarNameTxt.setText("" + getResources().getString(R.string.OrdersOnWay));
                                recyclerview.setVisibility(View.VISIBLE);
                                layout_empty.setVisibility(View.GONE);
                            } else {
                                GetListMyOrderWebService();
                            }
                        } else if ("False".equals(response.body().getStatus())) {
                            if (!TextUtils.isEmpty(message_str)) {
                                if (TextUtils.equals("Incorrect Access Token", message_str.trim())) {
                                    AppErrorsManager.showCustomErrorDialogNotCancel(HomeActivity.this,
                                            getResources().getString(R.string.error), message_str, new InstallCallback() {
                                                @Override
                                                public void onStatusDone(String status) {
                                                    Gson json = new Gson();
                                                    AppPreferences.saveString(HomeActivity.this, "userJson", "0");
                                                    Intent intent = new Intent(HomeActivity.this, SplashActivity.class);
                                                    startActivity(intent);
                                                    finish();
                                                }
                                            });
                                } else {
                                    AppErrorsManager.showCustomErrorDialog(HomeActivity.this,
                                            getResources().getString(R.string.error), message_str);
                                }
                            } else {
                                AppErrorsManager.showCustomErrorDialog(HomeActivity.this,
                                        getResources().getString(R.string.error), message_str);
                            }
                        }

                    } else {
                        AppErrorsManager.showCustomErrorDialog(HomeActivity.this,
                                getResources().getString(R.string.error), response.errorBody().toString());
                    }


                    mkLoader.setVisibility(View.GONE);

                }

                @Override
                public void onFailure(Call<OrdersResponse> call, Throwable t) {
                    AppErrorsManager.showCustomErrorDialog(HomeActivity.this,
                            getResources().getString(R.string.error), t.getMessage() + "");
                    mkLoader.setVisibility(View.GONE);
                }
            });
        }
    }


    private void GetDeliveryReceviedOrdersMenu() {
        toolbarNameTxt.setText("" + getResources().getString(R.string.MyFinishedOrder));
        recyclerview.setVisibility(View.VISIBLE);
        layout_empty.setVisibility(View.GONE);
        mkLoader.setVisibility(View.VISIBLE);
        User user = getUserDetials();
        if (user != null) {
            RetrofitWebService.getService(this).GetDeliveryReceviedOrdersList(user.getAccessToken()).enqueue(new Callback<OrdersResponse>() {
                @Override
                public void onResponse(Call<OrdersResponse> call, Response<OrdersResponse> response) {

                    if (response.code() == RootManager.RESPONSE_CODE_OK) {
                        String message_str = response.body().getMessage();
                        Log.e("messages", "GetMyOrdersList" + " " + message_str);
                        if ("True".equals(response.body().getStatus())) {

                            orderList = response.body().getOrders();
                            if (orderList.size() > 0) {
                                orderList = response.body().getOrders();
                                SetUpRecyclerOrders(orderList);
                                toolbarNameTxt.setText("" + getResources().getString(R.string.MyFinishedOrder));
                                recyclerview.setVisibility(View.VISIBLE);
                                layout_empty.setVisibility(View.GONE);
                            } else {
                                recyclerview.setVisibility(View.GONE);
                                layout_empty.setVisibility(View.VISIBLE);

                            }


                        } else if ("False".equals(response.body().getStatus())) {
                            if (!TextUtils.isEmpty(message_str)) {
                                if (TextUtils.equals("Incorrect Access Token", message_str.trim())) {
                                    AppErrorsManager.showCustomErrorDialogNotCancel(HomeActivity.this,
                                            getResources().getString(R.string.error), message_str, new InstallCallback() {
                                                @Override
                                                public void onStatusDone(String status) {
                                                    Gson json = new Gson();
                                                    AppPreferences.saveString(HomeActivity.this, "userJson", "0");
                                                    Intent intent = new Intent(HomeActivity.this, SplashActivity.class);
                                                    startActivity(intent);
                                                    finish();
                                                }
                                            });
                                } else {
                                    AppErrorsManager.showCustomErrorDialog(HomeActivity.this,
                                            getResources().getString(R.string.error), message_str);
                                }
                            } else {
                                AppErrorsManager.showCustomErrorDialog(HomeActivity.this,
                                        getResources().getString(R.string.error), message_str);
                            }
                        }

                    } else {
                        AppErrorsManager.showCustomErrorDialog(HomeActivity.this,
                                getResources().getString(R.string.error), response.errorBody().toString());
                    }


                    mkLoader.setVisibility(View.GONE);

                }

                @Override
                public void onFailure(Call<OrdersResponse> call, Throwable t) {
                    AppErrorsManager.showCustomErrorDialog(HomeActivity.this,
                            getResources().getString(R.string.error), t.getMessage() + "");
                    mkLoader.setVisibility(View.GONE);
                }
            });
        }
    }

    private void SetUpRecyclerOrders(final List<Order> orderList) {
        recyclerview.setVisibility(View.VISIBLE);
        ordersfilter = orderList;
        adapter = new RecyclerOrders(HomeActivity.this, ordersfilter, R.layout.row_list_order, -1,
                new OnItemClickTagListener() {
                    @Override
                    public void onItemClick(View view, int position, String tag) throws JSONException, FileNotFoundException {
                        //    Toast.makeText(MainActivity.this, "click", Toast.LENGTH_SHORT).show();
                        GetToMapDelevery(ordersfilter.get(position));
                    }
                });
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerview.setLayoutManager(linearLayoutManager);
        adapter.filterList(ordersfilter);
        recyclerview.setAdapter(adapter);


    }

    public boolean gps_enabled() {
        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;

        try {
            assert lm != null;
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ignored) {
        }

        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ignored) {
        }

        if (!gps_enabled && !network_enabled) {
            // notify user
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.setMessage(getResources().getString(R.string.gps_network_not_enabled));
            dialog.setPositiveButton(getResources().getString(R.string.open_location_settings), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    // TODO Auto-generated method stub
                    Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(myIntent);
                    //get gps
                }
            });
            dialog.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    // TODO Auto-generated method stub

                }
            });
            dialog.show();
            return false;

        } else {
            return true;
        }
    }

    private void GetToMapDelevery(Order order) {
        if (order != null) {
            fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
            PackageManager pm2 = getPackageManager();
            int hasPerm4 = pm2.checkPermission(Manifest.permission.ACCESS_FINE_LOCATION, getPackageName());
            if (hasPerm4 == PackageManager.PERMISSION_GRANTED) {
                if (gps_enabled() == true) {
                    fusedLocationClient.getLastLocation()
                            .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                                @Override
                                public void onSuccess(Location location) {

                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                                        startLocationButtonClick(location, order);
                                    } else {
                                        String str_order = new Gson().toJson(order, Order.class);
                                        Intent intent = new Intent(getApplicationContext(), MapsActivity.class);
                                        intent.putExtra("str_order", str_order);
                                        intent.putExtra("BillCode", order.getBillCode());

                                        if (location != null) {
                                            latitude = location.getLatitude();
                                            longitude = location.getLongitude();
                                            intent.putExtra("Mylatitude", latitude);
                                            intent.putExtra("Mylongitude", longitude);
                                            startActivityForResult(intent, 201);
                                        } else {
                                            if (latitude != 0.0 && longitude != 0.0) {
                                                intent.putExtra("Mylatitude", latitude);
                                                intent.putExtra("Mylongitude", longitude);
                                                startActivityForResult(intent, 201);
                                            } else {
                                                Toast.makeText(HomeActivity.this, "لم يتم تحديد عنوانك حتي الأن ! شغل خدمات الموقع وحاول مجددا", Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    }


                                }
                            });
                }
            }
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 1000:
                switch (resultCode) {
                    case Activity.RESULT_OK: {
                        // All required changes were successfully made
                        recreate();
                        break;
                    }
                    case Activity.RESULT_CANCELED: {
                        // The user was asked to change settings, but chose not to
                        Toast.makeText(this, getResources().getString(R.string.GPSAlertDialogMessage), Toast.LENGTH_LONG).show();
                        recreate();

                        break;
                    }
                    default: {
                        break;
                    }
                }
                break;
        }
        if (requestCode == 201 && resultCode == 201) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    GetOnWayOrdersListWebService();
                    swipeRefreshLayout.setRefreshing(false);
                }
            }, 2000);
        } else if (resultCode == 0) {
            CheckIsBatteryOptimization();
        }
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.nav_home) {
            GetOnWayOrdersListWebService();
        } else if (item.getItemId() == R.id.nav_new) {
            GetListMyOrderWebService();
        } else if (item.getItemId() == R.id.nav_finish) {
            GetDeliveryReceviedOrdersMenu();
        } else if (item.getItemId() == R.id.nav_Language) {
            openDialogLanguage();
        } else if (item.getItemId() == R.id.nav_logout) {
            logOut();
        } else if (item.getItemId() == R.id.nav_branchs) {
            GoToInfoActivity();
        }
        drawer.closeDrawer(GravityCompat.START);
        return false;
    }

    private void GoToInfoActivity() {
        Intent intent = new Intent(getApplicationContext(), InfoActivity.class);
        startActivity(intent);
    }


    public void logOut() {
        User user = getUserDetials();
        if (user != null) {
            AppErrorsManager.showSuccessDialog(HomeActivity.this, getString(R.string.logout), getString(R.string.do_you_want_logout), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Gson json = new Gson();
                    AppPreferences.saveString(HomeActivity.this, "userJson", "0");
                    Intent intent = new Intent(HomeActivity.this, SplashActivity.class);
                    startActivity(intent);
                    finish();

                }
            }, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                }
            });
        } else {
            Intent intent = new Intent(HomeActivity.this, SplashActivity.class);
            startActivity(intent);
        }


    }

    public void openDialogLanguage() {
        LayoutInflater factory = LayoutInflater.from(this);
        @SuppressLint("InflateParams") final View dialogView = factory.inflate(R.layout.select_language_dialog, null);
        final AlertDialog deleteDialog = new AlertDialog.Builder(this).create();
        LinearLayout fontLayout = dialogView.findViewById(R.id.layout_dialog);
        FontManager.applyFont(this, fontLayout);


        deleteDialog.setView(dialogView);

        dialogView.findViewById(R.id.arabicBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                changeLang("ar");
                AppLanguage.saveLanguage(HomeActivity.this, "ar");
                Intent intent = new Intent(getApplicationContext(), SplashActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                deleteDialog.dismiss();
            }
        });
        dialogView.findViewById(R.id.englishBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeLang("en");
                AppLanguage.saveLanguage(HomeActivity.this, "en");
                Intent intent = new Intent(getApplicationContext(), SplashActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                deleteDialog.dismiss();
            }
        });

        deleteDialog.show();
    }


    public void changeLang(String lang) {
        if (lang.equalsIgnoreCase(""))
            return;
        Locale myLocale = new Locale(lang);
        Locale.setDefault(myLocale);
        android.content.res.Configuration config = new android.content.res.Configuration();
        config.locale = myLocale;
        getResources().updateConfiguration(config, getResources().getDisplayMetrics());
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.text_action_go:
                openPowerSettings();
                break;
            case R.id.text_close_dialog:
                layout_screenLock.setVisibility(View.GONE);
                break;
        }
    }
}