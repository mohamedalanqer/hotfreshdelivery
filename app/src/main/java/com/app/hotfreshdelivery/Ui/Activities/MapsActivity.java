package com.app.hotfreshdelivery.Ui.Activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.graphics.PorterDuff;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Looper;
import android.os.PowerManager;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.hotfreshdelivery.Callback.InstallCallback;
import com.app.hotfreshdelivery.Callback.OnItemClickTagListener;
import com.app.hotfreshdelivery.Manager.AppErrorsManager;
import com.app.hotfreshdelivery.Manager.AppLanguage;
import com.app.hotfreshdelivery.Manager.AppPreferences;
import com.app.hotfreshdelivery.Manager.FontManager;
import com.app.hotfreshdelivery.Manager.RootManager;
import com.app.hotfreshdelivery.Modules.Install.Branch;
import com.app.hotfreshdelivery.Modules.ItemService;
import com.app.hotfreshdelivery.Modules.LatLongHistory;
import com.app.hotfreshdelivery.Modules.Map.Legs;
import com.app.hotfreshdelivery.Modules.Map.Routes;
import com.app.hotfreshdelivery.Modules.ModelDeleveryFB;
import com.app.hotfreshdelivery.Modules.OnlineOrder;
import com.app.hotfreshdelivery.Modules.Order;
import com.app.hotfreshdelivery.Modules.User;
import com.app.hotfreshdelivery.R;
import com.app.hotfreshdelivery.Ui.Adapters.OrderFoodListAdapter;
import com.app.hotfreshdelivery.Ui.Adapters.RecyclerServiceDialog;
import com.app.hotfreshdelivery.directionhelpers.FetchURL;
import com.app.hotfreshdelivery.directionhelpers.TaskLoadedCallback;
import com.app.hotfreshdelivery.locationtracker.BackgroundLocationService;
import com.app.hotfreshdelivery.webService.RetrofitWebService;
import com.app.hotfreshdelivery.webService.model.response.LoginResponse;
import com.app.hotfreshdelivery.webService.model.response.OrderResponse;
import com.app.hotfreshdelivery.webService.model.response.RootResponse;
import com.app.hotfreshdelivery.webService.model.response.RoutesResponse;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.karumi.dexter.BuildConfig;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import com.tuyenmonkey.mkloader.MKLoader;

import org.json.JSONException;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URLEncoder;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.os.PowerManager.PARTIAL_WAKE_LOCK;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, TaskLoadedCallback, LocationListener {
    FirebaseFirestore db;
    CollectionReference ref;
    Map<String, String> mapHash = new HashMap<>();
    private GoogleMap mMap;
    private Location mLocation;
    private double latitudeKitchen, longitudeKitchen;
    private double longitudeOrder, latitudeOrder;
    private double Mylatitude, Mylongitude;
    private FusedLocationProviderClient fusedLocationProviderClient = null;
    private MarkerOptions Kitchen = null, Customer = null;
    public static MarkerOptions CustomerStatic = null;
    private Polyline currentPolyline;
    static final int REQUST_CODE = 1000;
    int zoomMap = 17;
    private Order order = null;
    private List<OnlineOrder> onlineOrders = new ArrayList<>();
    private RecyclerView recyclerView;
    OrderFoodListAdapter adapter;


    private String BillCode = "";
    MKLoader mkLoader;

    String NameKichen = "Kitchen";
    String NameCustomer = "Customer";
    boolean FirstTime = false;
    LocationManager locationManager;
    String provider;
    static MapsActivity instance;
    private User user = null;


    public static MapsActivity getInstance() {
        return instance;
    }

    TextView text_name_user, text_delivery_date, text_delivery_time, text_address, text_kitchenName;
    TextView text_price_total, text_mobile, text_whatsup;
    TextView text_duratoion, text_status;
    Button btn_action;
    private TextView text_PaymentType, text_ID;
    String Mode = "driving";
    String statusPreferences = "";


    PowerManager powerManager;
    PowerManager.WakeLock wakeLock;
    public static boolean locationStarted = false;

    private String getCollectionName() {
        return order.getBillCode() + "";
    }

    @SuppressLint("InvalidWakeLockTag")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        AppLanguage.setContentLang(this);
        setContentView(R.layout.activity_bottom_sheet_content);
        View layout = findViewById(R.id.layout);
        user = getUserDetails();
        if (locationStarted == false) {
            final Intent intent = new Intent(this, BackgroundLocationService.class);
            Objects.requireNonNull(this).startService(intent);
            bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);


        }
        FontManager.applyFont(this, layout);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        statusPreferences = AppPreferences.getString(this, getResources().getString(R.string.statusPreferences), getResources().getString(R.string.status1));


        BillCode = getIntent().getStringExtra("BillCode");
        String str_order = getIntent().getStringExtra("str_order");
        if (!TextUtils.isEmpty(str_order)) {
            order = new Gson().fromJson(str_order, Order.class);
        }
        registerForLocationUpdates();


        setUpView();

        powerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
        wakeLock = powerManager.newWakeLock(PARTIAL_WAKE_LOCK,
                "Debug");
        wakeLock.acquire();
    }

    private void GetOrderByIdWebService(String billCode) {
        mkLoader.setVisibility(View.VISIBLE);
        RetrofitWebService.getService(this).ShowOrdersById(billCode).enqueue(new Callback<OrderResponse>() {
            @Override
            public void onResponse(Call<OrderResponse> call, Response<OrderResponse> response) {
                Log.e("GetOrderDetials", response.toString());
                if (RootManager.RESPONSE_CODE_OK == response.code()) {
                    if (TextUtils.equals("True", response.body().getStatus())) {
                        if (response.body() != null) {
                            OrderResponse response1 = response.body();
                            setOrderToView(response1);

                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                                startLocationButtonClick();
                            }


                        } else {
                            AppErrorsManager.showCustomErrorDialog(MapsActivity.this, getResources().getString(R.string.error),
                                    response.body().getMessage());
                        }
                    } else {
                        AppErrorsManager.showCustomErrorDialog(MapsActivity.this, getResources().getString(R.string.error),
                                response.body().getMessage());
                    }
                } else if (TextUtils.equals("False", response.body().getStatus())) {
                    AppErrorsManager.showCustomErrorDialog(MapsActivity.this, getResources().getString(R.string.error),
                            response.body().getMessage());
                } else {
                    AppErrorsManager.showCustomErrorDialog(MapsActivity.this, getResources().getString(R.string.Failure),
                            getResources().getString(R.string.InternalServerError));
                }
                mkLoader.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<OrderResponse> call, Throwable t) {
                mkLoader.setVisibility(View.GONE);
                AppErrorsManager.showCustomErrorDialog(MapsActivity.this, getResources().getString(R.string.Failure),
                        t.getMessage() + "");
            }
        });

    }


    private void setUpView() {
        text_name_user = findViewById(R.id.text_name_user);
        text_delivery_date = findViewById(R.id.text_delivery_date);
        text_delivery_time = findViewById(R.id.text_delivery_time);
        text_address = findViewById(R.id.text_address);
        text_kitchenName = findViewById(R.id.text_kitchenName);
        text_price_total = findViewById(R.id.text_price_total);
        text_mobile = findViewById(R.id.text_mobile);
        text_whatsup = findViewById(R.id.text_whatsup);
        text_PaymentType = findViewById(R.id.text_PaymentType);
        mkLoader = findViewById(R.id.mkLoader);
        btn_action = findViewById(R.id.btn_action);
        text_duratoion = findViewById(R.id.text_duratoion);
        text_status = findViewById(R.id.text_status);
        text_ID = findViewById(R.id.text_ID);
        recyclerView = findViewById(R.id.recyclerview);


        Mylatitude = getIntent().getDoubleExtra("Mylatitude", 0);
        Mylongitude = getIntent().getDoubleExtra("Mylongitude", 0);
        // Getting LocationManager object
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        // Creating an empty criteria object
        Criteria criteria = new Criteria();
        // Getting the name of the provider that meets the criteria
        provider = locationManager.getBestProvider(criteria, false);
        if (provider != null && !provider.equals("")) {

            PackageManager pm2 = getPackageManager();
            int hasPerm4 = pm2.checkPermission(Manifest.permission.ACCESS_FINE_LOCATION, getPackageName());
            if (hasPerm4 == PackageManager.PERMISSION_GRANTED) {
                Location location = locationManager.getLastKnownLocation(provider);

                locationManager.requestLocationUpdates(provider, 20000, 1, this);


                if (location != null)
                    onLocationChanged(location);
                else
                    Toast.makeText(getBaseContext(), "Location can't be retrieved", Toast.LENGTH_SHORT).show();

            } else
                checkAndRequestPermissionsLocation();
        }
        latitudeKitchen = getIntent().getDoubleExtra("latitudeKitchen", 0);
        longitudeKitchen = getIntent().getDoubleExtra("longitudeKitchen", 0);
        latitudeOrder = getIntent().getDoubleExtra("latitudeOrder", 0);
        longitudeOrder = getIntent().getDoubleExtra("longitudeOrder", 0);

        registerForLocationUpdates();


        SetUpBottomSheet();
        db = FirebaseFirestore.getInstance();
        ref = db.collection("Delevery::").document(getCollectionName()).collection(getCollectionName());
        mapHash.put("userId", order.getBillCode() + "");
        mapHash.put("userName", order.getClientName() + "");
        mapHash.put("DeliveryId", getUserDetails().getID() + "");
        mapHash.put("DeliveryName", getUserDetails().getName() + "");
        mapHash.put("DeliveryPic", "" + getUserDetails().getImage());
        mapHash.put("DeliveryMobile", "" + getUserDetails().getMobile());
        mapHash.put("OrderId", getCollectionName());
        mapHash.put("DeliveryLate", Mylatitude + "");
        mapHash.put("DeliveryLang", Mylongitude + "");


        db.collection("Delevery::").document(getCollectionName()).collection(getCollectionName())
                .whereEqualTo("OrderId", getCollectionName()).get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                if (queryDocumentSnapshots.size() == 0) {
                    FirstTime = true;
                    AppPreferences.saveString(MapsActivity.this, getResources().getString(R.string.statusPreferences), getResources().getString(R.string.status0));
                    statusPreferences = getResources().getString(R.string.status0);

                } else {
                    List<ModelDeleveryFB> data = queryDocumentSnapshots.toObjects(ModelDeleveryFB.class);
                    for (int i = 0; i < data.size(); i++) {
                        if (data.get(i).getDrationAndDistance() != null) {
                            text_duratoion.setText("" + data.get(i).getDrationAndDistance());
                        }

                        if (data.get(i).getTrackingList() != null) {
                            if (data.get(i).getTrackingList().size() > 0) {
                                List<LatLongHistory> latLngListTraking = data.get(i).getTrackingList();
                                PolyLineHistory(latLngListTraking);
                            }
                        }
                    }
                }
            }
        });

        btn_action.setVisibility(View.GONE);
        if (!TextUtils.isEmpty(BillCode)) {
            GetOrderByIdWebService(BillCode);
        } else {
            if (order != null) {
                GetOrderByIdWebService(order.getBillCode());
            }
        }


    }

    private String StatusOrderDelivery = "";

    private void setOrderToView(OrderResponse order) {
        StatusOrderDelivery = order.getDeliveryStatus();

        if (order.getDeliveryStatus() != null) {
            if (!TextUtils.isEmpty(order.getDeliveryStatus())) {
                text_status.setText("" + order.getDeliveryStatus());
            }
        }

        String Lang_Status = "";
        if (TextUtils.equals("Received", order.getDeliveryStatus())) {
            btn_action.setVisibility(View.VISIBLE);
            btn_action.setText("" + getResources().getString(R.string.ReceiptThisOrder));
            btn_action.getBackground().setColorFilter(getResources().getColor(R.color.green), PorterDuff.Mode.MULTIPLY);
            Lang_Status = getResources().getString(R.string.S_Received);
        } else if (TextUtils.equals("OnWay", order.getDeliveryStatus())) {
            AppPreferences.saveString(this, "OrderIdPref", order.getBillCode());
            btn_action.setVisibility(View.VISIBLE);
            btn_action.setText("" + getResources().getString(R.string.DeliveredThisOrder));
            btn_action.getBackground().setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);
            Lang_Status = getResources().getString(R.string.S_OnWay);
        } else if (TextUtils.equals("Customer Recevied", order.getDeliveryStatus())) {
            btn_action.setVisibility(View.GONE);
            Lang_Status = getResources().getString(R.string.S_CustomerRecevied);
        } else if (TextUtils.equals("CustomerRecevied", order.getDeliveryStatus())) {
            btn_action.setVisibility(View.GONE);
            Lang_Status = getResources().getString(R.string.S_CustomerRecevied);
        } else {
            btn_action.setVisibility(View.GONE);
        }
        if (!TextUtils.isEmpty(Lang_Status)) {
            text_status.setText("" + Lang_Status);
        }

        text_ID.setText("#" + order.getBillCode());

        text_PaymentType.setText("" + order.getPaymentType());
        if (TextUtils.equals(order.getPaymentType() + "", RootManager.PaymentType_cash)) {
            text_PaymentType.setText("" + getResources().getString(R.string.payment_cash));
        } else if (TextUtils.equals(order.getPaymentType() + "", RootManager.PaymentType_visa)) {
            text_PaymentType.setText("" + getResources().getString(R.string.payment_visa));
        } else if (TextUtils.equals(order.getPaymentType() + "", RootManager.PaymentType_wallet)) {
            text_PaymentType.setText("" + getResources().getString(R.string.payment_wallet));
        }

        NameKichen = order.getCompany();
        NameCustomer = order.getCustName();
        text_name_user.setText(order.getCustName() + "");
        text_price_total.setText(" (" + getResources().getString(R.string.ThePrice) + "  " + order.getTotal() + " " + getResources().getString(R.string.Saudi_riyal) + ")");
        text_kitchenName.setText("" + NameKichen);
        text_delivery_date.setText("" + order.getReceivedDate());
        if (order.getReceivedTime() != null) {
            if (!TextUtils.isEmpty(order.getReceivedTime()))
                text_delivery_time.setText(" " + order.getReceivedTime());
        }
        /**  if (!TextUtils.isEmpty(order.getCustAddress())) {
         text_address.setText("" + order.getCustAddress());
         } else {
         text_address.setText("" + getResources().getString(R.string.undefined));
         }*/


        if (order.getOrders() != null) {
            if (order.getOrders().size() > 0) {
                onlineOrders = order.getOrders();
                setupRecycler(onlineOrders);
            }
        }

        // text_mobile.setText("" + order.getCustMobile());
        text_mobile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (order.getCustMobile() != null) {
                    String mobile = order.getMobile();

                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", mobile, null));
                    startActivity(intent);
                }
            }
        });

        text_whatsup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (order.getCustMobile() != null) {
                    openWhatsApp(order.getCustMobile(), getResources().getString(R.string.message_send_whatsup));
                }
            }
        });
        if (!TextUtils.isEmpty(order.getCompany())) {
            Branch branch = GetBranchByNameCompany(order.getCompany());
            if (branch != null) {
                if (branch.getLat() != null)
                    latitudeKitchen = Double.parseDouble(branch.getLat());
                if (branch.getLong() != null)
                    longitudeKitchen = Double.parseDouble(branch.getLong());
            }

        }
        if (!TextUtils.isEmpty(order.getLat()))
            latitudeOrder = Double.parseDouble(order.getLat());

        if (!TextUtils.isEmpty(order.getLong()))
            longitudeOrder = Double.parseDouble(order.getLong());


        Log.e("LATLOJGORDER", latitudeKitchen + " , " + longitudeOrder);


        if (Kitchen == null || Customer == null) {
            Kitchen = new MarkerOptions().position(new LatLng(latitudeKitchen, longitudeKitchen)).icon(BitmapDescriptorFactory.fromResource(R.drawable.location)).title(NameKichen);
            Customer = new MarkerOptions().position(new LatLng(latitudeOrder, longitudeOrder)).icon(BitmapDescriptorFactory.fromResource(R.drawable.user)).title(NameCustomer);
            CustomerStatic = Customer;
            new FetchURL(MapsActivity.this).execute(getUrl(Kitchen.getPosition(), Customer.getPosition(), "driving"), "driving");
            if (mMap != null) {
                Marker marker = mMap.addMarker(Kitchen);
                Marker marker2 = mMap.addMarker(Customer);
                marker.setTag(order.getCompany());
                marker2.setTag(order.getCustName());
            }
            text_address.setOnClickListener(view -> {
                openMapGoogle(CustomerStatic);
            });
            if (!TextUtils.equals("Customer Recevied", order.getDeliveryStatus())) {
                if (FirstTime == true) {
                    GetDrationAndDistanceBetweenCustomerKitchen(Kitchen.getPosition(), Customer.getPosition(), "driving", 5, 20);
                } else {
                    GetDrationAndDistanceBetweenDeliveryCutomer(new LatLng(Mylatitude, Mylongitude), Customer.getPosition(), "driving", 0, 0);
                }
            }


        }


        btn_action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                if (TextUtils.equals("Received", order.getDeliveryStatus())) {
                    ChangeStatusOrderById(order.getBillCode(), RootManager.S_Delivery_Way, "");
                } else if (TextUtils.equals("OnWay", order.getDeliveryStatus())) {
                    if (TextUtils.equals(order.getPaymentType() + "", RootManager.PaymentType_cash)) {
                        AppErrorsManager.showTwoActionDialog(MapsActivity.this, getResources().getString(R.string.DeliveredThisOrder), getResources().getString(R.string.DeliveredThisOrderMessage),
                                getResources().getString(R.string.Complete), new InstallCallback() {
                                    @Override
                                    public void onStatusDone(String status) {
                                        if (TextUtils.equals(status, RootManager.PaymentType_cash_Hand)) {
                                            ChangeStatusOrderById(order.getBillCode(), RootManager.S_Delivery_Recevied, RootManager.PaymentType_cash_Hand);
                                        } else if (TextUtils.equals(status, RootManager.PaymentType_Global_Hand)) {
                                            ChangeStatusOrderById(order.getBillCode(), RootManager.S_Delivery_Recevied, RootManager.PaymentType_Global_Hand);

                                        }
                                    }
                                });
                    } else {
                        ChangeStatusOrderById(order.getBillCode(), RootManager.S_Delivery_Recevied, "");
                    }

                } else if (TextUtils.equals("Customer Recevied", order.getDeliveryStatus())) {
                    btn_action.setVisibility(View.GONE);
                } else if (TextUtils.equals("CustomerRecevied", order.getDeliveryStatus())) {
                    btn_action.setVisibility(View.GONE);
                } else {
                    btn_action.setVisibility(View.GONE);
                }
            }
        });

        /**  List<String> listTackingPref = AppPreferences.getLocationIds(this, order.getBillCode()+"");
         if (listTackingPref != null) {
         Log.e("listTackingPref",listTackingPref.toString());
         if (listTackingPref.size() > 0) {
         Log.e("listTackingPref",listTackingPref.size() +" size");
         List<LatLongHistory> latLngs = new ArrayList<>();
         for (int i = 0; i < listTackingPref.size(); i++) {
         Log.e("listTackingPref",i +"  i");
         String latlngStr = listTackingPref.get(i);
         if (latlngStr != null) {
         LatLongHistory latLng = new Gson().fromJson(latlngStr, LatLongHistory.class);
         latLngs.add(latLng);
         }
         }
         if (latLngs.size() > 0) {
         SaveListTrackingFireBase(latLngs);
         }
         }

         }*/
        if (getUserDetails() != null) {
            if (TextUtils.equals(order.getDeliveryID(), getUserDetails().getID())) {
                if (TextUtils.equals(order.getDeliveryStatus(), "OnWay")) {
                    AddDataOrderFirstTimeFireBase(order, Mylongitude, Mylatitude);
                    if (isNetworkAvailable()) {
                        if (locationStarted == false) {
                            final Intent intent = new Intent(this, BackgroundLocationService.class);
                            Objects.requireNonNull(this).startService(intent);
                            bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
                        }


                    } else {
                        noInternetAvailable();
                    }

                } else if (TextUtils.equals("Customer Recevied", order.getDeliveryStatus())) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        stopLocationButtonClick();
                    }
                }

            }
        }
/**
 List<String> listTackingPref = AppPreferences.getLocationIds(this, "2637108");
 if (listTackingPref != null) {
 if (listTackingPref.size() > 0) {
 List<LatLng> latLngs = new ArrayList<>();
 for (int i = 0; i < listTackingPref.size(); i++) {
 String latlngStr = listTackingPref.get(i);
 if (latlngStr != null) {
 LatLng latLng = new Gson().fromJson(latlngStr, LatLng.class);
 latLngs.add(latLng);
 }
 }
 if (latLngs.size() > 0)
 SaveListTrackingFireBase(latLngs);

 }

 }*/
    }

    private void noInternetAvailable() {
        final Dialog dialognonet = new Dialog(this);
        dialognonet.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialognonet.setContentView(R.layout.no_internet);
        dialognonet.setCancelable(true);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            lp.copyFrom(Objects.requireNonNull(dialognonet.getWindow()).getAttributes());
        }
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialognonet.findViewById(R.id.bt_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isNetworkAvailable())
                    dialognonet.dismiss();
                else
                    dialognonet.setCancelable(false);
            }
        });
        dialognonet.show();
        Objects.requireNonNull(dialognonet.getWindow()).setAttributes(lp);
    }

    private void openSettings() {
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", BuildConfig.APPLICATION_ID, null);
        intent.setData(uri);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void startLocationButtonClick() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            if (this.checkPermission()) {
                askPermission();
            }
        }


        PackageManager pm2 = getPackageManager();
        int hasPerm4 = pm2.checkPermission(Manifest.permission.ACCESS_FINE_LOCATION, getPackageName());
        if (hasPerm4 == PackageManager.PERMISSION_GRANTED) {
            gpsService.startTracking();
            mTracking = true;
            locationStarted = true;
            toggleButtons();
        } else
            openSettings();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void stopLocationButtonClick() {
        mTracking = false;
        locationStarted = false;
        gpsService.stopTracking();
        toggleButtons();
    }

    private boolean checkPermission() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_BACKGROUND_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            return false;
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            return ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_BACKGROUND_LOCATION) != PackageManager.PERMISSION_GRANTED;
        }
        return true;
    }

    private void askPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            ActivityCompat.requestPermissions(Objects.requireNonNull(this), new String[]{Manifest.permission.ACCESS_BACKGROUND_LOCATION}, 1);
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void toggleButtons() {


        if (mTracking) {
            /**  @SuppressLint("UseRequireInsteadOfGet") final Snackbar snackBar = Snackbar.make(Objects.requireNonNull(getView()).getRootView().findViewById(android.R.id.content), "Tracking Started", Snackbar.LENGTH_SHORT);
            snackBar.setAction("OK", new View.OnClickListener() {
            @Override public void onClick(View v) {
            snackBar.dismiss();
            }
            });
            snackBar.show();*/
        }
    }

    private ServiceConnection serviceConnection = new ServiceConnection() {

        public void onServiceConnected(ComponentName className, IBinder service) {

            String name = className.getClassName();
            Log.e("nameservvv", name + " FF");
            if (name.endsWith("BackgroundLocationService")) {
                gpsService = ((BackgroundLocationService.LocationServiceBinder) service).getService();
                if (!mTracking) {
                    if (locationStarted) {

                    } else {

                    }
                }
            }
        }

        public void onServiceDisconnected(ComponentName className) {
            if (className.getClassName().equals("BackgroundLocationService")) {
                gpsService = null;
            }
        }
    };

    private BackgroundLocationService gpsService;
    private boolean mTracking = false;

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = null;
        if (connectivityManager != null) {
            activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        }
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public void setupRecycler(List<OnlineOrder> onlineOrders) {
        adapter = new OrderFoodListAdapter(getApplicationContext(), onlineOrders, -1, new OnItemClickTagListener() {
            @Override
            public void onItemClick(View view, int position, String tag) throws JSONException, FileNotFoundException {
                OnlineOrder item = onlineOrders.get(position);
                if (TextUtils.equals("service", tag)) {
                    ShowDialogServiceItem(item.getItemServices());
                }
                adapter.notifyDataSetChanged();

            }
        });
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext(), RecyclerView.HORIZONTAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);

    }

    private void ShowDialogServiceItem(List<ItemService> itemServices) {

        for (int i = 0; i < itemServices.size(); i++) {
            itemServices.get(i).setIs_selected(true);
        }
        RecyclerServiceDialog adapter_service = new RecyclerServiceDialog(this, itemServices, R.layout.row_item_service, new OnItemClickTagListener() {
            @Override
            public void onItemClick(View view, int position, String tag) throws JSONException, FileNotFoundException {
            }
        });

        AppErrorsManager.showServiceListDialog(MapsActivity.this, adapter_service, getResources().getString(R.string.close), new InstallCallback() {
            @Override
            public void onStatusDone(String status) {
                if (TextUtils.equals("yes", status)) {
                }

            }
        });

    }


    private Branch GetBranchByNameCompany(String company) {
        List<Branch> branchList = new ArrayList<>();
        Branch branch = null;
        String branchesJson = AppPreferences.getString(MapsActivity.this, "branchesJson");

        if (branchesJson != null) {
            branchList = new Gson().fromJson(branchesJson, new TypeToken<List<Branch>>() {
            }.getType());
        }

        for (int i = 0; i < branchList.size(); i++) {
            if (TextUtils.equals(company, branchList.get(i).getCompany())) {
                branch = branchList.get(i);
                return branch;
            }
        }
        return branch;
    }


    private void ChangeStatusOrderById(String billCode, int s_delivery_way, String PaymentType) {
        mkLoader.setVisibility(View.VISIBLE);
        if (s_delivery_way == RootManager.S_Delivery_Way) {
            AppPreferences.saveString(this, "OrderIdPref", billCode);
            AppPreferences.saveString(this, getResources().getString(R.string.statusPreferences), getResources().getString(R.string.status1));
            statusPreferences = getResources().getString(R.string.status1);
            text_status.setText("" + statusPreferences);
        } else if (s_delivery_way == RootManager.S_Delivery_Recevied) {
            AppPreferences.saveString(this, getResources().getString(R.string.statusPreferences), getResources().getString(R.string.status2));
            AppPreferences.saveString(this, "OrderIdPref", null);
            statusPreferences = getResources().getString(R.string.status2);
            text_status.setText("" + statusPreferences);

            List<String> listTackingPref = AppPreferences.getLocationIds(this, billCode);
            if (listTackingPref != null) {
                if (listTackingPref.size() > 0) {
                    List<LatLongHistory> latLngs = new ArrayList<>();
                    for (int i = 0; i < listTackingPref.size(); i++) {
                        String latlngStr = listTackingPref.get(i);
                        if (latlngStr != null) {
                            LatLongHistory latLng = new Gson().fromJson(latlngStr, LatLongHistory.class);
                            latLngs.add(latLng);
                        }
                    }
                    if (latLngs.size() > 0)
                        SaveListTrackingFireBase(latLngs);

                }

            }
        }
        user = getUserDetails();
        if (user != null) {
            RetrofitWebService.getService(this).ChangeOrderStatus(user.getAccessToken(), billCode, s_delivery_way, PaymentType).enqueue(new Callback<RootResponse>() {
                @Override
                public void onResponse(Call<RootResponse> call, Response<RootResponse> response) {
                    IsUpdate = true;

                    if (RootManager.RESPONSE_CODE_OK == response.code()) {
                        if (response.body() != null) {

                            RootResponse rootResponse = response.body();
                            GetOrderByIdWebService(billCode);
                            if (TextUtils.equals("True", rootResponse.getStatus())) {
                                if (s_delivery_way == RootManager.S_Delivery_Recevied) {
                                    AppErrorsManager.showCompleteNotCancel(MapsActivity.this, rootResponse.getMessage() + ""
                                            , new InstallCallback() {
                                                @Override
                                                public void onStatusDone(String status) {
                                                    StatusOrderDelivery = "CustomerRecevied";
                                                    onBackPressed();
                                                }
                                            });
                                } else {
                                    AppErrorsManager.showCustomErrorDialogNotCancel(MapsActivity.this, getResources().getString(R.string.info),
                                            rootResponse.getMessage(), new InstallCallback() {
                                                @Override
                                                public void onStatusDone(String status) {

                                                }
                                            });
                                }
                            } else {
                                AppErrorsManager.showCustomErrorDialog(MapsActivity.this, getResources().getString(R.string.error),
                                        rootResponse.getMessage());
                            }
                        } else {
                            AppErrorsManager.showCustomErrorDialog(MapsActivity.this, getResources().getString(R.string.Failure),
                                    getResources().getString(R.string.InternalServerError));
                        }
                    } else {
                        AppErrorsManager.showCustomErrorDialog(MapsActivity.this, getResources().getString(R.string.Failure),
                                getResources().getString(R.string.InternalServerError));
                    }

                    if (response.errorBody() != null) {
                        try {
                            Log.e("SetRetriveOrder", " error code: " + response.code() + " error content: " + response.errorBody().string().toString());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    mkLoader.setVisibility(View.GONE);
                }

                @Override
                public void onFailure(Call<RootResponse> call, Throwable t) {
                    mkLoader.setVisibility(View.GONE);

                }
            });
        }
    }

    boolean IsUpdate = false;


    BottomSheetBehavior bottomSheetBehavior;

    private void SetUpBottomSheet() {
        LinearLayout llBottomSheet = findViewById(R.id.bottom_sheet);
        bottomSheetBehavior = BottomSheetBehavior.from(llBottomSheet);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        bottomSheetBehavior.setPeekHeight(60);
        bottomSheetBehavior.setHideable(true);
        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });
    }

    public User getUserDetails() {
        User user = new User();
        Gson gson = new Gson();
        String json = AppPreferences.getString(this, "userJson");
        if (json != null) {
            if (!TextUtils.equals("0", json)) {
                return gson.fromJson(json, User.class);
            }

        }
        return null;
    }

    int DistanceRange = 150;


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;


        // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));

        PackageManager pm2 = getPackageManager();
        int hasPerm4 = pm2.checkPermission(Manifest.permission.ACCESS_FINE_LOCATION, getPackageName());
        if (hasPerm4 == PackageManager.PERMISSION_GRANTED) {
            getLocation();

        } else {
            checkAndRequestPermissionsLocation();
        }
        mMap = googleMap;

        googleMap.getUiSettings().setMapToolbarEnabled(true);
        googleMap.getUiSettings().setCompassEnabled(true);
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);
        googleMap.setMyLocationEnabled(true);


        if (mMap != null) {
            LatLng latLng = new LatLng(Mylatitude, Mylongitude);
            //move map camera
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoomMap));
            mMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
                @Override
                public void onCameraIdle() {
                    Log.e("zoom", mMap.getCameraPosition().zoom + "");
                }
            });

            mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

                // Use default InfoWindow frame
                @Override
                public View getInfoWindow(Marker args) {
                    return null;
                }

                // Defines the contents of the InfoWindow
                @Override
                public View getInfoContents(Marker args) {
                    View v = getLayoutInflater().inflate(R.layout.info_window_layout, null);
                    TextView title = v.findViewById(R.id.tvTitle);
                    title.setText(args.getTitle());
                    mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                        public void onInfoWindowClick(Marker marker) {
                            OnclickMarkerIcon(marker);


                        }
                    });

                    return v;

                }
            });
        }
    }


    @SuppressLint("MissingPermission")
    void registerForLocationUpdates() {
        fusedLocationProviderClient = getFusedLocationProviderClient();
        LocationRequest locationRequest = LocationRequest.create();
        Looper looper = Looper.myLooper();
        fusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, looper);
    }

    @NonNull
    private FusedLocationProviderClient getFusedLocationProviderClient() {
        if (fusedLocationProviderClient == null) {
            fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        }
        return fusedLocationProviderClient;
    }


    Marker mCurrLocationMarker;

    private LocationCallback locationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            super.onLocationResult(locationResult);
            Location lastLocation = locationResult.getLastLocation();
            Log.i("MapsActivity", "Location: " + lastLocation.getLatitude() + " " + lastLocation.getLongitude());

            if (mCurrLocationMarker != null) {
                mCurrLocationMarker.remove();
            }
            if (mMap != null) {
                Mylatitude = lastLocation.getLatitude();
                Mylongitude = lastLocation.getLongitude();

                LatLng latLng = new LatLng(Mylatitude, Mylongitude);
                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.position(latLng);

                //move map camera
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoomMap));

            }
            Log.e("locationdata", lastLocation.getLatitude() + " // " + lastLocation.getLongitude());

        }
    };


    private String getUrl(LatLng origin, LatLng dest, String directionMode) {
        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;
        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;
        // Mode
        String mode = "mode=" + directionMode;
        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + mode;
        // Output format
        String output = "json";
        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters + "&key=" + getString(R.string.google_maps_key2);
        return url;
    }


    private void PolyLineHistory(List<LatLongHistory> result) {
        ArrayList<LatLng> points;
        PolylineOptions lineOptions = null;
        for (int i = 0; i < result.size(); i++) {
            points = new ArrayList<>();
            lineOptions = new PolylineOptions();
            // Fetching all the points in i-th route
            for (int j = 0; j < result.size(); j++) {
                LatLongHistory point = result.get(j);
                double lat = point.getLatitude();
                double lng = point.getLongitude();
                LatLng position = new LatLng(lat, lng);
                points.add(position);
            }
            // Adding all the points in the route to LineOptions
            lineOptions.addAll(points);
            lineOptions.width(20);
            lineOptions.color(getResources().getColor(R.color.colorGreyDark));
        }

        // Drawing polyline in the Google Map for the i-th route
        if (lineOptions != null) {
            mMap.addPolyline(lineOptions);
        }
    }

    @Override
    public void onTaskDone(Object... values) {
        if (currentPolyline != null)
            currentPolyline.remove();
        currentPolyline = mMap.addPolyline((PolylineOptions) values[0]);
    }


    public void getLocation() {
        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;

        try {
            assert lm != null;
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ignored) {
        }

        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ignored) {
        }

        if (!gps_enabled && !network_enabled) {
            // notify user
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.setMessage(getResources().getString(R.string.gps_network_not_enabled));
            dialog.setPositiveButton(getResources().getString(R.string.open_location_settings), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    // TODO Auto-generated method stub
                    Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(myIntent);
                    //get gps
                }
            });
            dialog.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    // TODO Auto-generated method stub

                }
            });
            dialog.show();
        } else {
            GetLocationUser();
        }
    }


    public void GetLocationUser() {
        PackageManager pm2 = getPackageManager();
        int hasPerm4 = pm2.checkPermission(Manifest.permission.ACCESS_FINE_LOCATION, getPackageName());
        if (hasPerm4 == PackageManager.PERMISSION_GRANTED) {
            fusedLocationProviderClient.getLastLocation()
                    .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            // Got last known location. In some rare situations this can be null.
                            if (location != null) {
                                Mylatitude = location.getLatitude();
                                Mylongitude = location.getLongitude();
                            }
                        }
                    });

        } else {
            checkAndRequestPermissionsLocation();
        }

    }

    private void checkAndRequestPermissionsLocation() {
        String[] permissions = new String[]{
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION
        };
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String permission : permissions) {
            if (ContextCompat.checkSelfPermission(getApplicationContext(), permission) != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(permission);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUST_CODE);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUST_CODE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    GetLocationUser();
                } else {
                    onBackPressed();
                }
                break;
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.e("mmmmm", location.getLatitude() + "dsdfsdfdsds");
        if (mMap != null) {
            Mylatitude = location.getLatitude();
            Mylongitude = location.getLongitude();
            if (mCurrLocationMarker != null) {
                mCurrLocationMarker.remove();
            }
            LatLng latLng = new LatLng(Mylatitude, Mylongitude);
            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(latLng);

            CameraUpdate center =
                    CameraUpdateFactory.newLatLng(latLng);
            CameraUpdate zoom = CameraUpdateFactory.zoomTo(zoomMap);

            mMap.moveCamera(center);
            mMap.animateCamera(zoom);


        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    private void OnclickMarkerIcon(Marker marker) {

        if (marker.getTag() != null) {
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

        }
    }

    private int TimeToWay = 0;
    private float distanceToWay = 0f;

    private void GetDrationAndDistanceBetweenCustomerKitchen(LatLng origin, LatLng dest, String directionMode, final int durationOld, final float distance_floatOld) {
        String str_origin = "" + origin.latitude + "," + origin.longitude;
        // Destination of route
        String str_dest = "" + dest.latitude + "," + dest.longitude;
        RetrofitWebService.getService(this).GetDurations(getString(R.string.google_maps_key2)
                , true, "en", str_origin, str_dest, directionMode).enqueue(new Callback<RoutesResponse>() {
            @Override
            public void onResponse(Call<RoutesResponse> call, Response<RoutesResponse> response) {
                Log.e("GetDurations", " CustomerKitchen " + response.toString());
                if (response.code() == RootManager.RESPONSE_CODE_OK) {
                    int duration = 1;
                    float distance_float = 0f;
                    if (response.body() != null) {
                        List<Routes> routes = response.body().getRoutes();
                        for (int i = 0; i < routes.size(); i++) {
                            List<Legs> legs = routes.get(i).legs;
                            for (int l = 0; l < legs.size(); l++) {
                                int distance = legs.get(l).getDistance().getValue();
                                duration = legs.get(l).getDuration().getValue();
                                distance_float = distance / 1000f;
                                text_duratoion.setText("" + duration / 60 + " " + getResources().getString(R.string.min) + " | " + distance_float + " " + getResources().getString(R.string.KM));
                            }
                            int timeNew = duration + durationOld;
                            float WayNew = distance_float + distance_floatOld;
                            TimeToWay = timeNew / 60;
                            distanceToWay = WayNew;
                            String text_disc = "" + timeNew / 60 + " " + getResources().getString(R.string.min) + " | " + WayNew + " " + getResources().getString(R.string.KM);
                            SaveNewDrationDistanceFireBase(text_disc, TimeToWay + "", distanceToWay + "");
                            text_duratoion.setText("" + timeNew / 60 + " " + getResources().getString(R.string.min) + " | " + WayNew + " " + getResources().getString(R.string.KM));
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<RoutesResponse> call, Throwable t) {
                Log.e("GetDurations", t.getMessage() + "");

            }
        });
    }

    private void GetDrationAndDistanceBetweenDeliveryCutomer(LatLng origin, LatLng dest, String directionMode, final int durationOld, final float distance_floatOld) {
        String str_origin = "" + origin.latitude + "," + origin.longitude;
        // Destination of route
        String str_dest = "" + dest.latitude + "," + dest.longitude;
        RetrofitWebService.getService(this).GetDurations(getString(R.string.google_maps_key2)
                , true, "en", str_origin, str_dest, directionMode).enqueue(new Callback<RoutesResponse>() {
            @Override
            public void onResponse(Call<RoutesResponse> call, Response<RoutesResponse> response) {
                if (response.code() == RootManager.RESPONSE_CODE_OK) {
                    int duration = 1;
                    float distance_float = 0f;
                    if (response.body() != null) {
                        List<Routes> routes = response.body().getRoutes();
                        for (int i = 0; i < routes.size(); i++) {
                            List<Legs> legs = routes.get(i).legs;
                            for (int l = 0; l < legs.size(); l++) {
                                int distance = legs.get(l).getDistance().getValue();
                                duration = legs.get(l).getDuration().getValue();
                                distance_float = distance / 1000f;
                                text_duratoion.setText("" + duration / 60 + " " + getResources().getString(R.string.min) + " | " + distance_float + " " + getResources().getString(R.string.KM));
                            }
                            int timeNew = duration + durationOld;
                            float WayNew = distance_float + distance_floatOld;
                            TimeToWay = timeNew / 60;
                            distanceToWay = WayNew;
                            String text_disc = "" + timeNew / 60 + " " + getResources().getString(R.string.min) + " | " + WayNew + " " + getResources().getString(R.string.KM);
                            SaveNewDrationDistanceFireBase(text_disc, TimeToWay + "", distanceToWay + "");
                            text_duratoion.setText("" + timeNew / 60 + " " + getResources().getString(R.string.min) + " | " + WayNew + " " + getResources().getString(R.string.KM));
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<RoutesResponse> call, Throwable t) {
                Log.e("GetDurations", t.getMessage() + "");

            }
        });
    }

    public static void SerivceDistanceDeliveryCutomer(Context context, LatLng origin, LatLng dest, String directionMode, FirebaseFirestore db, String OderID) {
        String str_origin = "" + origin.latitude + "," + origin.longitude;
        String str_dest = "" + dest.latitude + "," + dest.longitude;


        Long OldTimeHistory = AppPreferences.getLong(context, "OldTimeHistory");
        Long now = new Date().getTime();
        Long defnow = 0L;
        if (OldTimeHistory == 0) {
            AppPreferences.saveLong(context, "OldTimeHistory", now);
            defnow = 0L;
        } else {
            defnow = now - OldTimeHistory;
        }
        if (defnow >= 120000) {
            RetrofitWebService.getService(context).GetDurations(context.getString(R.string.google_maps_key2)
                    , true, "en", str_origin, str_dest, directionMode).enqueue(new Callback<RoutesResponse>() {
                @Override
                public void onResponse(Call<RoutesResponse> call, Response<RoutesResponse> response) {
                    Log.e("GetDurations", "Serivce  CustomerKitchen " + response.toString());
                    if (response.code() == RootManager.RESPONSE_CODE_OK) {
                        int duration = 1;
                        float distance_float = 0f;
                        if (response.body() != null) {
                            List<Routes> routes = response.body().getRoutes();
                            for (int i = 0; i < routes.size(); i++) {
                                List<Legs> legs = routes.get(i).legs;
                                for (int l = 0; l < legs.size(); l++) {
                                    int distance = legs.get(l).getDistance().getValue();
                                    duration = legs.get(l).getDuration().getValue();
                                    distance_float = distance / 1000f;
                                }
                                int timeNew = duration;
                                float WayNew = distance_float;
                                int TimeToWay = timeNew / 60;
                                float distanceToWay = WayNew;
                                String text_disc = "" + timeNew / 60 + " " + context.getResources().getString(R.string.min) + " | " + WayNew + " " + context.getResources().getString(R.string.KM);
                                db.collection("Delevery::").document(OderID).collection(OderID)
                                        .document(OderID).update(
                                        "DrationAndDistance", text_disc
                                        , "Dration", timeNew
                                        , "Distance", TimeToWay)
                                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                if (task.isSuccessful()) {

                                                }
                                            }
                                        });
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<RoutesResponse> call, Throwable t) {
                    Log.e("GetDurations", t.getMessage() + "");

                }
            });

            AppPreferences.saveLong(context, "OldTimeHistory", now);
        }


    }

    private void AddDataOrderFirstTimeFireBase(OrderResponse order, Double lng, Double late) {
        if (order != null) {
            if (!TextUtils.isEmpty(order.getBillCode())) {
                statusPreferences = AppPreferences.getString(this, getResources().getString(R.string.statusPreferences), getResources().getString(R.string.status2));
                if (TextUtils.equals("OnWay", order.getDeliveryStatus())) {
                    mapHash.put("DeliveryLate", lng + "");
                    mapHash.put("DeliveryLang", late + "");
                    mapHash.put("DrationAndDistance", "" + text_duratoion.getText().toString());
                    mapHash.put("Dration", "" + TimeToWay);
                    mapHash.put("Distance", "" + distanceToWay);
                    mapHash.put("Status", "" + statusPreferences);
                    db.collection("Delevery::").document(getCollectionName()).collection(getCollectionName())
                            .document(getCollectionName()).set(mapHash)
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {

                                    }
                                }
                            });
                }
            }
        }
    }

    private void SaveListTrackingFireBase(List<LatLongHistory> list) {
        if (!FirstTime) {
            statusPreferences = AppPreferences.getString(this, getResources().getString(R.string.statusPreferences), getResources().getString(R.string.status2));
            db.collection("Delevery::").document(getCollectionName()).collection(getCollectionName())
                    .document(getCollectionName()).update("TrackingList", list, "Status", statusPreferences)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                AppPreferences.clearLocationIds(MapsActivity.this, getCollectionName());
                            }
                        }
                    });
        }

    }

    private void SaveNewDrationDistanceFireBase(String DrationAndDistance, String Dration, String Distance) {
        if (!FirstTime) {
            db.collection("Delevery::").document(getCollectionName()).collection(getCollectionName())
                    .document(getCollectionName()).update(
                    "DrationAndDistance", DrationAndDistance
                    , "Dration", Dration
                    , "Distance", Distance)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {

                            }
                        }
                    });
        }

    }


    @Override
    public void onBackPressed() {
        if (TextUtils.equals("OnWay", StatusOrderDelivery)) {
            AppErrorsManager.showLogOutsDialogNotCancel(MapsActivity.this, getResources().getString(R.string.Warning),
                    getResources().getString(R.string.Warning_service_gps), new InstallCallback() {
                        @Override
                        public void onStatusDone(String status) {
                            if (TextUtils.equals("Exit", status)) {
                                ExitApp();
                            }

                        }
                    });
        } else {
            if (IsUpdate) {
                Intent intent = new Intent();
                intent.putExtra("result", "update");
                setResult(201, intent);
                finish();
            } else
                super.onBackPressed();
        }


    }

    private void ExitApp() {
        if (IsUpdate) {
            Intent intent = new Intent();
            intent.putExtra("result", "update");
            setResult(201, intent);
            finish();
        } else
            super.onBackPressed();
    }


    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onStop() {
        super.onStop();

    }

    @Override
    protected void onPause() {
        super.onPause();


    }

    @Override
    protected void onRestart() {
        super.onRestart();

    }


    @Override
    protected void onStart() {
        if (locationStarted == true) {
            final Intent intent = new Intent(this, BackgroundLocationService.class);
            Objects.requireNonNull(this).startService(intent);
            bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);


        }
        super.onStart();
    }

    @Override
    protected void onDestroy() {
        wakeLock.release();
        if (serviceConnection != null) {
            unbindService(serviceConnection);
            gpsService.stopService(new Intent(this, BackgroundLocationService.class));
            mTracking = false;
        }
        super.onDestroy();

    }


    private void openWhatsApp(String numero, String mensaje) {

        try {
            PackageManager packageManager = getPackageManager();
            Intent i = new Intent(Intent.ACTION_VIEW);
            String url = "https://api.whatsapp.com/send?phone=" + numero + "&text=" + URLEncoder.encode(mensaje, "UTF-8");
            i.setPackage("com.whatsapp");
            i.setData(Uri.parse(url));
            if (i.resolveActivity(packageManager) != null) {
                startActivity(i);
            } else {
                Toast.makeText(this, "" + getString(R.string.no_whatsapp), Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            Log.e("ERROR WHATSAPP", e.toString());
            Toast.makeText(this, "" + getString(R.string.no_whatsapp), Toast.LENGTH_SHORT).show();
        }

    }

    private void openMapGoogle(MarkerOptions markerOptions) {
        if (markerOptions != null) {
            String strUri = "http://maps.google.com/maps?q=loc:" + markerOptions.getPosition().latitude + "," + markerOptions.getPosition().longitude + " (" + markerOptions.getTitle() + ")";
            Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(strUri));
            intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
            startActivity(intent);
        } else {
            Toast.makeText(this, "" + getString(R.string.CustomerNotHaveLocation), Toast.LENGTH_SHORT).show();
        }

    }

}




