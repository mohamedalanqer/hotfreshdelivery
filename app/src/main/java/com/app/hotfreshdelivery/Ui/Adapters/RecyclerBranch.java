package com.app.hotfreshdelivery.Ui.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;


import com.app.hotfreshdelivery.Callback.OnItemClickTagListener;
import com.app.hotfreshdelivery.Manager.FontManager;
import com.app.hotfreshdelivery.Modules.Install.Branch;
import com.app.hotfreshdelivery.R;

import org.json.JSONException;

import java.io.FileNotFoundException;
import java.util.List;


public class RecyclerBranch extends RecyclerView.Adapter<RecyclerBranch.CustomView> {
    Context context;
    List<Branch> mylist;
    private OnItemClickTagListener listener;
    int Layout;
    public static int ItemSelect = -1 ;


    public class CustomView extends RecyclerView.ViewHolder {
        TextView text_name , text_map;
        ImageView img_select ;
        TextView edit_address ;

        public CustomView(View v) {
            super(v);
            FontManager.applyFont(context, v);
            text_name = v.findViewById(R.id.text_name);
            text_map = v.findViewById(R.id.text_map);
            edit_address = v.findViewById(R.id.edit_address);
            img_select = v.findViewById(R.id.img_select);
         }

    }

    public RecyclerBranch() {
    }

    public RecyclerBranch(Context context, List<Branch> mylist, int ItemSelect , int Layout, OnItemClickTagListener listener) {
        this.context = context;
        this.mylist = mylist;
        this.listener = listener;
        this.Layout = Layout;
        this.ItemSelect  = ItemSelect ;
    }

    @Override
    public CustomView onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(Layout, parent, false);
        CustomView viewholder = new CustomView(view);
        return viewholder;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(final CustomView holder, final int position) {
        holder.text_name.setText(mylist.get(position).getName() + "");
        holder.edit_address.setText(mylist.get(position).getAddress() + "");



        holder.text_map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    listener.onItemClick(view, position, "map");
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        });
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    listener.onItemClick(view, position, "select");
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return mylist.size();
    }


}

