package com.app.hotfreshdelivery.Ui.Activities;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.app.hotfreshdelivery.Manager.AppErrorsManager;
import com.app.hotfreshdelivery.Manager.AppLanguage;
import com.app.hotfreshdelivery.Manager.AppPreferences;
import com.app.hotfreshdelivery.Manager.FontManager;
import com.app.hotfreshdelivery.Manager.RootManager;
import com.app.hotfreshdelivery.Modules.Install.Branch;
import com.app.hotfreshdelivery.Modules.Install.Definition;
import com.app.hotfreshdelivery.Modules.User;
import com.app.hotfreshdelivery.R;
import com.app.hotfreshdelivery.webService.RetrofitWebService;
import com.app.hotfreshdelivery.webService.model.response.InstallResponse;
import com.app.hotfreshdelivery.webService.model.response.LoginResponse;
import com.google.gson.Gson;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashActivity extends AppCompatActivity {
    /**
     * Duration of wait
     **/
    private final int SPLASH_DISPLAY_LENGTH = 500;
    private Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, Window.FEATURE_NO_TITLE);
        AppLanguage.setContentLang(this);
        setContentView(R.layout.activity_splash);
        FontManager.applyFont(this, findViewById(R.id.layout));
        handler = new Handler(Looper.getMainLooper());


        initSetUp();
    }

    @Override
    public void applyOverrideConfiguration(Configuration overrideConfiguration) {
        if (overrideConfiguration != null) {
            int uiMode = overrideConfiguration.uiMode;
            overrideConfiguration.setTo(getBaseContext().getResources().getConfiguration());
            overrideConfiguration.uiMode = uiMode;
        }
        super.applyOverrideConfiguration(overrideConfiguration);
    }

    boolean isGuideShow = false;

    private void initSetUp() {


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                /* Create an Intent that will start the Menu-Activity. */
                GetInstalDataWebService();

            }
        }, SPLASH_DISPLAY_LENGTH);
    }

    private void GetInstalDataWebService() {
        String TaxValue = "5";
        AppPreferences.saveString(SplashActivity.this, "taxJson", TaxValue + "");
        RetrofitWebService.getService(this).GetInstall().enqueue(new Callback<InstallResponse>() {
            @Override
            public void onResponse(Call<InstallResponse> call, final Response<InstallResponse> response) {
                Log.e("GetInstall", response.toString());
                if (RootManager.RESPONSE_CODE_OK == response.code()) {
                    if (response.body() != null) {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                if (response.body().getDefinition() != null) {
                                    if (TextUtils.equals("False", response.body().getDefinition().getCloseAppStatus())) {

                                        List<Branch> branches = response.body().getBranches();
                                        if (branches != null) {
                                            String branchesJson = new Gson().toJson(branches);
                                            AppPreferences.saveString(SplashActivity.this, "branchesJson", branchesJson + "");
                                        }

                                        Definition definitions = response.body().getDefinition();
                                        if (definitions != null) {
                                            String definitionsJson = new Gson().toJson(definitions);
                                            AppPreferences.saveString(SplashActivity.this, "definitionsJson", definitionsJson + "");
                                        }

                                        User user = getUserDetials();
                                        if (user != null) {
                                            Intent mainIntent = new Intent(SplashActivity.this, HomeActivity.class);
                                            mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                            startActivity(mainIntent);
                                            finish();
                                            //GetMyProfile(user);
                                        } else {
                                            Intent mainIntent = new Intent(SplashActivity.this, LoginActivity.class);
                                            mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                            startActivity(mainIntent);
                                            finish();
                                        }
                                    }
                                }

                            }
                        });


                    } else {
                        AppErrorsManager.showCustomErrorDialog(SplashActivity.this, getResources().getString(R.string.Failure),
                                getResources().getString(R.string.InternalServerError));
                    }
                } else {
                    AppErrorsManager.showCustomErrorDialog(SplashActivity.this, getResources().getString(R.string.Failure),
                            getResources().getString(R.string.InternalServerError));
                }
            }

            @Override
            public void onFailure(Call<InstallResponse> call, Throwable t) {

                AppErrorsManager.showCustomErrorDialog(SplashActivity.this, getResources().getString(R.string.Failure),
                        t.getMessage() + "");
            }
        });

    }

    public User getUserDetials() {
        Gson gson = new Gson();
        String json = AppPreferences.getString(SplashActivity.this, "userJson");
        if (json != null) {
            if (!TextUtils.equals("0", json)) {
                return gson.fromJson(json, User.class);
            }

        }
        return null;
    }

    private void GetMyProfile(User user) {
        if (user != null) {
            final String old_token = user.getAccessToken();
            RetrofitWebService.getService(this).GetMyProfile(user.getAccessToken()).enqueue(new Callback<LoginResponse>() {
                @Override
                public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                    Log.e("GetMyProfile", response.toString());
                    if (RootManager.RESPONSE_CODE_OK == response.code()) {
                        if (response.body() != null) {
                            if (TextUtils.equals("True", response.body().getStatus())) {

                                User user = response.body().getUser();
                                user.setAccessToken(old_token);
                                if (user != null) {

                                    Gson json = new Gson();
                                    String userJson = json.toJson(user);
                                    AppPreferences.saveString(SplashActivity.this, "userJson", userJson);

                                    Intent mainIntent = new Intent(SplashActivity.this, HomeActivity.class);
                                    mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(mainIntent);
                                    finish();


                                } else {
                                    Intent mainIntent = new Intent(SplashActivity.this, LoginActivity.class);
                                    mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(mainIntent);
                                    finish();
                                }
                            } else if (TextUtils.equals("False", response.body().getStatus())) {
                                Intent mainIntent = new Intent(SplashActivity.this, LoginActivity.class);
                                mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(mainIntent);
                                finish();
                            }
                        } else {
                            Intent mainIntent = new Intent(SplashActivity.this, LoginActivity.class);
                            mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(mainIntent);
                            finish();
                        }
                    } else {
                        AppErrorsManager.showCustomErrorDialog(SplashActivity.this, getResources().getString(R.string.Failure),
                                getResources().getString(R.string.InternalServerError));
                    }
                }

                @Override
                public void onFailure(Call<LoginResponse> call, Throwable t) {
                    Log.e("GetMyProfile", t.toString());
                    AppErrorsManager.showCustomErrorDialog(SplashActivity.this, getResources().getString(R.string.Failure),
                            t.getMessage() + "");
                }
            });
        }
    }
}
