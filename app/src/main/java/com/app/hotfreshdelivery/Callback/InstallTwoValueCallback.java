package com.app.hotfreshdelivery.Callback;


public interface InstallTwoValueCallback {
    void onStatusDone(int value1, String value2);
}
