package com.app.hotfreshdelivery.Manager;


import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Build;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.app.hotfreshdelivery.R;

import java.util.ArrayList;
import java.util.List;


public class RootManager {

    public static final String URL = "http://cn.sa/HatService/New/";
    public static final String URL_INSTALL = "http://cn.sa/HatService/";
    public static final String IMAGE_URL = "http://cn.sa/HatService/";

    private static final String ROOT = "fonts/",
            FONTAWESOME = ROOT + "cairo_regular.ttf";
    public static final String KEY_LANG_AR = "ar";
    private static final String KEY_LANG_EN_US = "en-us";
    private static final String KEY_LANG_EN = "en";
    //new url saad
    public static final String URL_Root_New = "http://delivery.nanosoft-tec.com/";
    public static final String KEY_LANG_AR_New = "ar";
    private static final String KEY_LANG_EN_US_New = "en";
    public static final String IMAGE_URL_New = "http://delivery.nanosoft-tec.com/";


    public static final String PaymentType_cash_Hand = "PayCash";
    public static final String PaymentType_Global_Hand = "Global";


    public static final String PaymentType_cash = "Cash";
    public static final String PaymentType_visa = "Visa";
    public static final String PaymentType_wallet = "Wallet";


    public static final int S_Delivery_Way = 0;
    public static final int S_Delivery_Recevied =1;

    public static final int Follow_User = 1;
    public static final int Follow_Advertisement = 2;
    public static final int Follow_Category = 3;


    public static final String Type_Customer = "1";
    public static final String Type_Provider = "2";
    public static final String device = "android";
    public static boolean backToMyListProfile = false;


    public static String Start_Date_Booking = "";
    public static String End_Date_Booking = "";
    public static int Payment_method = 1;
    public static List<Fragment> fragmentList = new ArrayList<>();
    public static String NameUser_Booking = "";
    public static String Mobile_Booking = "";



    public static int Order_New = 0;
    public static int Order_Accept = 1;
    public static int Order_Reject = 2;
    public static int Order_Cancel = 3;




    public final static int RESPONSE_CODE_OK = 200;
    public final static String RESPONSE_STATUS_OK = "OK";
    public final static int RESPONSE_CODE_CREATED = 201;
    public final static String RESPONSE_STATUS_CREATED = "Created";
    public final static int RESPONSE_CODE_NO_CONTENT = 204;
    public final static String RESPONSE_STATUS_NO_CONTENT = "No Content";
    public final static int RESPONSE_CODE_BAD_REQUEST = 400;
    public final static String RESPONSE_STATUS_BAD_REQUEST = "Bad Request";
    public final static int RESPONSE_CODE_UNAUTHORIZED = 401;
    public final static String RESPONSE_STATUS_UNAUTHORIZED = "Unauthorized";
    public final static int RESPONSE_CODE_FORBIDDEN = 403;
    public final static String RESPONSE_STATUS_FORBIDDEN = "Forbidden";
    public final static int RESPONSE_CODE_NOT_FOUND = 404;
    public final static String RESPONSE_STATUS_NOT_FOUND = "Not Found";
    public final static int RESPONSE_CODE_CONFLICT = 409;
    public final static String RESPONSE_STATUS_CONFLICT = "Conflict";
    public final static int RESPONSE_CODE_UNPROCESSABLE_ENTITY = 422;
    public final static String RESPONSE_STATUS_UNPROCESSABLE_ENTITY = "Unprocessable Entity";

    public final static int RESPONSE_ORDER_STATUS_PENDING = 0;
    public final static int RESPONSE_ORDER_STATUS_ACCEPTED = 1;
    public final static int RESPONSE_ORDER_STATUS_START_TRIP = 2;
    public final static int RESPONSE_ORDER_STATUS_END_TRIP = 3;
    public final static int RESPONSE_ORDER_STATUS_DECLINED = 4;



    public static final String Stats_uncertain = "Uncertain";
    public static final String Stats_certain = "Certain";
    public static final String Stats_ready = "Ready";
    public static final String Stats_updated = "Updated";
    public static final String Stats_recevied = "Recevied";

    public static final String GetMessageStatusByTagName(String tagName, Context context, ImageView img_status) {
        Log.e("tagtag", tagName + "");
        String message = "";
        if (TextUtils.equals(tagName, Stats_certain)) {
            message = context.getResources().getString(R.string.m_certain);

        } else if (TextUtils.equals(tagName, Stats_uncertain)) {
            message = context.getResources().getString(R.string.m_uncertain);

        } else if (TextUtils.equals(tagName, Stats_ready)) {
            message = context.getResources().getString(R.string.m_ready);

        } else if (TextUtils.equals(tagName, Stats_updated)) {
            message = context.getResources().getString(R.string.m_updated);

        } else if (TextUtils.equals(tagName, Stats_recevied)) {
            message = context.getResources().getString(R.string.m_recevied);

        }
        return message;
    }


}
