package com.app.hotfreshdelivery.Manager;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.app.hotfreshdelivery.Callback.InstallCallback;
import com.app.hotfreshdelivery.R;
import com.app.hotfreshdelivery.Ui.Adapters.RecyclerServiceDialog;

import java.util.List;

import static android.content.Intent.ACTION_VIEW;
import static android.content.Intent.CATEGORY_BROWSABLE;


public class AppErrorsManager {


    public static void showErrorDialog(Context context, String error) {
        if (context != null) {
            AlertDialog alertDialogBuilder = new AlertDialog.Builder(context)
                    .setTitle(context.getString(R.string.error))
                    .setMessage(error)
                    .setPositiveButton(context.getString(R.string.ok), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }).show();

            TextView textView = alertDialogBuilder.findViewById(android.R.id.message);
            TextView alertTitle = alertDialogBuilder.findViewById(R.id.alertTitle);
            Button button1 = alertDialogBuilder.findViewById(android.R.id.button1);
            Button button2 = alertDialogBuilder.findViewById(android.R.id.button2);
            if (textView != null) {
                textView.setLineSpacing(0, 1.5f);
            }

        }
    }


    public static void showErrorDialog(Activity context, String error, DialogInterface.OnClickListener okClickListener) {
        if (context != null && !context.isFinishing()) {
            AlertDialog alertDialogBuilder = new AlertDialog.Builder(context)
                    .setTitle(R.string.error).setMessage(error).setPositiveButton(R.string.ok, okClickListener).show();

            TextView textView = alertDialogBuilder.findViewById(android.R.id.message);
            TextView alertTitle = alertDialogBuilder.findViewById(R.id.alertTitle);
            Button button1 = alertDialogBuilder.findViewById(android.R.id.button1);
            Button button2 = alertDialogBuilder.findViewById(android.R.id.button2);
            if (textView != null) {
                textView.setLineSpacing(0, 1.5f);
            }

        }
    }


    public static void showSuccessDialog(Activity context, String title, String error,
                                         DialogInterface.OnClickListener okClickListener,
                                         DialogInterface.OnClickListener cancelClickListener) {
        if (context != null && !context.isFinishing()) {
            AlertDialog alertDialogBuilder = new AlertDialog.Builder(context)
                    .setTitle(title)
                    .setMessage(error)
                    .setPositiveButton(context.getString(R.string.confirm), okClickListener).setNegativeButton(context.getString(R.string.cancel), cancelClickListener)
                    .show();

            TextView textView = alertDialogBuilder.findViewById(android.R.id.message);
            TextView alertTitle = alertDialogBuilder.findViewById(R.id.alertTitle);
            Button button1 = alertDialogBuilder.findViewById(android.R.id.button1);
            Button button2 = alertDialogBuilder.findViewById(android.R.id.button2);
            if (textView != null) {
                textView.setLineSpacing(0, 1.5f);
            }

        }
    }

    public static void showDescriptionDialog(Activity context, String title, String error,
                                             DialogInterface.OnClickListener okClickListener) {
        if (context != null && !context.isFinishing()) {
            AlertDialog alertDialogBuilder = new AlertDialog.Builder(context)
                    .setTitle(title)
                    .setMessage(error)
                    .setPositiveButton(context.getString(R.string.close), okClickListener)
                    .show();

            TextView textView = alertDialogBuilder.findViewById(android.R.id.message);
            TextView alertTitle = alertDialogBuilder.findViewById(R.id.alertTitle);
            Button button1 = alertDialogBuilder.findViewById(android.R.id.button1);
            Button button2 = alertDialogBuilder.findViewById(android.R.id.button2);
            if (textView != null) {
                textView.setLineSpacing(0, 1.5f);
            }

        }
    }


    public static void showSuccessDialog(Activity context, String error,
                                         DialogInterface.OnClickListener okClickListener) {
        if (context != null && !context.isFinishing()) {
            AlertDialog alertDialogBuilder = new AlertDialog.Builder(context)
                    .setTitle(context.getString(R.string.info))
                    .setMessage(error)
                    .setPositiveButton(context.getString(R.string.ok), okClickListener)
                    .show();

            TextView textView = alertDialogBuilder.findViewById(android.R.id.message);
            TextView alertTitle = alertDialogBuilder.findViewById(R.id.alertTitle);
            Button button1 = alertDialogBuilder.findViewById(android.R.id.button1);
            Button button2 = alertDialogBuilder.findViewById(android.R.id.button2);
            if (textView != null) {
                textView.setLineSpacing(0, 1.5f);
            }

        }
    }


    public static void showCustomErrorDialog(Activity context, String title, String error) {
        LayoutInflater inflater = context.getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.dialog_custom_error_layout, null);
        final AlertDialog alertDialogBuilder = new AlertDialog.Builder(context).create();
        alertDialogBuilder.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        alertDialogBuilder.setView(dialoglayout);
        TextView text_title_error_dialog = dialoglayout.findViewById(R.id.text_title_error_dialog);
        TextView text_body_error_dialog = dialoglayout.findViewById(R.id.text_body_error_dialog);
        TextView close_dialog = dialoglayout.findViewById(R.id.close_dialog);
        FontManager.applyFont(context, dialoglayout);

        close_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogBuilder.dismiss();
            }
        });

        if (!title.isEmpty())
            text_title_error_dialog.setText(title);
        text_body_error_dialog.setText(error);
        if (!((Activity) context).isFinishing()) {
            if (context != null)
                try {
                    alertDialogBuilder.show();
                } catch (WindowManager.BadTokenException e) {
                    //use a log message
                }
        }

    }


    public static void showCustomErrorDialogNotCancel(Activity context, String title, String error, final InstallCallback installCallback) {
        LayoutInflater inflater = context.getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.dialog_custom_error_layout, null);
        final AlertDialog alertDialogBuilder = new AlertDialog.Builder(context).create();
        alertDialogBuilder.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        alertDialogBuilder.setView(dialoglayout);
        TextView text_title_error_dialog = dialoglayout.findViewById(R.id.text_title_error_dialog);
        TextView text_body_error_dialog = dialoglayout.findViewById(R.id.text_body_error_dialog);
        TextView close_dialog = dialoglayout.findViewById(R.id.close_dialog);

        FontManager.applyFont(context, dialoglayout);

        close_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogBuilder.dismiss();
                installCallback.onStatusDone("Close");
            }
        });


        if (!title.isEmpty())
            text_title_error_dialog.setText(title);
        text_body_error_dialog.setText(error);
        if (!((Activity) context).isFinishing()) {
            if (context != null)
                try {
                    alertDialogBuilder.setCancelable(false);
                    alertDialogBuilder.show();
                } catch (WindowManager.BadTokenException e) {
                    //use a log message
                }
        }

    }


    public static void showCompleteNotCancel(Activity context, String title, final InstallCallback installCallback) {
        LayoutInflater inflater = context.getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.dialog_complete_order_layout, null);
        final AlertDialog alertDialogBuilder = new AlertDialog.Builder(context).create();
        alertDialogBuilder.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        alertDialogBuilder.setView(dialoglayout);
        TextView text_title_error_dialog = dialoglayout.findViewById(R.id.text_title_error_dialog);
        TextView close_dialog = dialoglayout.findViewById(R.id.close_dialog);

        FontManager.applyFont(context, dialoglayout);

        close_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogBuilder.dismiss();
                installCallback.onStatusDone("Close");
            }
        });


        if (!title.isEmpty())
            text_title_error_dialog.setText(title);
        if (!((Activity) context).isFinishing()) {
            if (context != null)
                try {
                    alertDialogBuilder.setCancelable(false);
                    alertDialogBuilder.show();
                } catch (WindowManager.BadTokenException e) {
                    //use a log message
                }
        }

    }


    public static void showCustomErrorDialog(Activity context, String title, String error, final InstallCallback callback) {
        LayoutInflater inflater = context.getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.dialog_custom_error_layout, null);
        final AlertDialog alertDialogBuilder = new AlertDialog.Builder(context).create();
        alertDialogBuilder.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        alertDialogBuilder.setView(dialoglayout);
        TextView text_title_error_dialog = dialoglayout.findViewById(R.id.text_title_error_dialog);
        TextView text_body_error_dialog = dialoglayout.findViewById(R.id.text_body_error_dialog);
        TextView close_dialog = dialoglayout.findViewById(R.id.close_dialog);
        FontManager.applyFont(context, dialoglayout);

        close_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogBuilder.dismiss();
                callback.onStatusDone("close");
            }
        });
        if (!title.isEmpty())
            text_title_error_dialog.setText(title);
        text_body_error_dialog.setText(error);
        if (!((Activity) context).isFinishing()) {
            if (context != null)
                try {
                    alertDialogBuilder.show();
                } catch (WindowManager.BadTokenException e) {
                    //use a log message
                }
        }

    }

    public static void showServiceListDialog(Activity context, RecyclerServiceDialog adapter, String btn_str, final InstallCallback callback) {
        LayoutInflater inflater = context.getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.dialog_service_layout, null);
        final AlertDialog alertDialogBuilder = new AlertDialog.Builder(context).create();
        alertDialogBuilder.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        alertDialogBuilder.setView(dialoglayout);
        RecyclerView recycler_view = dialoglayout.findViewById(R.id.recycler_view);

        TextView text_yes = dialoglayout.findViewById(R.id.text_yes);
        ImageView img_close = dialoglayout.findViewById(R.id.img_close);
        TextView text_body_title_dialog = dialoglayout.findViewById(R.id.text_body_title_dialog);

        FontManager.applyFont(context, dialoglayout);


        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        recycler_view.setLayoutManager(linearLayoutManager);
        recycler_view.setAdapter(adapter);

        text_yes.setText("" + btn_str);
        text_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onStatusDone("yes");
                alertDialogBuilder.dismiss();
            }
        });


        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onStatusDone("no");
                alertDialogBuilder.dismiss();

            }
        });

        if (!((Activity) context).isFinishing()) {
            if (context != null)
                try {
                    alertDialogBuilder.show();
                } catch (WindowManager.BadTokenException e) {
                    //use a log message
                }
        }

    }

    public static void showLogOutsDialogNotCancel(Activity context, String title, String error, final InstallCallback installCallback) {
        LayoutInflater inflater = context.getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.dialog_back_layout, null);
        final AlertDialog alertDialogBuilder = new AlertDialog.Builder(context).create();
        alertDialogBuilder.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        alertDialogBuilder.setView(dialoglayout);
        TextView text_title_error_dialog = dialoglayout.findViewById(R.id.text_title_error_dialog);
        TextView text_body_error_dialog = dialoglayout.findViewById(R.id.text_body_error_dialog);
        TextView close_dialog = dialoglayout.findViewById(R.id.close_dialog);
        TextView close_logout = dialoglayout.findViewById(R.id.close_logout);

        FontManager.applyFont(context, dialoglayout);

        close_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogBuilder.dismiss();
                installCallback.onStatusDone("Close");
            }
        });
        close_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogBuilder.dismiss();
                installCallback.onStatusDone("Exit");
            }
        });

        if (!title.isEmpty())
            text_title_error_dialog.setText(title);
        text_body_error_dialog.setText(error);
        if (!((Activity) context).isFinishing()) {
            if (context != null)
                try {
                    alertDialogBuilder.setCancelable(true);
                    alertDialogBuilder.show();
                } catch (WindowManager.BadTokenException e) {
                    //use a log message
                }
        }

    }

    /**
     * public static void showOrderFoodsListDialog(Activity context, String title, String error, List<OrderFoods> orderFoods, Order order, OrderFoodListAdapter imagesListAdapter) {
     * LayoutInflater inflater = context.getLayoutInflater();
     * View dialoglayout = inflater.inflate(R.layout.dialog_orderdata_list_layout, null);
     * final AlertDialog alertDialogBuilder = new AlertDialog.Builder(context).create();
     * alertDialogBuilder.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
     * <p>
     * alertDialogBuilder.setView(dialoglayout);
     * TextView text_title_error_dialog = dialoglayout.findViewById(R.id.text_title_error_dialog);
     * ImageView close_dialog = dialoglayout.findViewById(R.id.close_dialog);
     * TextView text_price_total, text_price_discount, text_price_delivery, text_price_food, text_price_extra;
     * text_price_total = dialoglayout.findViewById(R.id.text_price_total);
     * text_price_discount = dialoglayout.findViewById(R.id.text_price_discount);
     * text_price_delivery = dialoglayout.findViewById(R.id.text_price_delivery);
     * text_price_food = dialoglayout.findViewById(R.id.text_price_food);
     * text_price_extra = dialoglayout.findViewById(R.id.text_price_extra);
     * <p>
     * text_price_total.setText(""+order.getTotal_cost()+" "+order.getCurrency_symbol());
     * text_price_discount.setText("-"+order.getDiscount_cost()+" "+order.getCurrency_symbol());
     * text_price_delivery.setText(""+order.getDelivery_cost()+" "+order.getCurrency_symbol());
     * text_price_food.setText(""+order.getFoods_cost()+" "+order.getCurrency_symbol());
     * text_price_extra.setText(""+order.getExtra_cost()+" "+order.getCurrency_symbol());
     * <p>
     * <p>
     * RecyclerView recyclerView = dialoglayout.findViewById(R.id.recyclerview);
     * imagesListAdapter = new OrderFoodListAdapter(context, orderFoods, -1 );
     * <p>
     * LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
     * recyclerView.setLayoutManager(linearLayoutManager);
     * recyclerView.setAdapter(imagesListAdapter);
     * <p>
     * close_dialog.setOnClickListener(new View.OnClickListener() {
     *
     * @Override public void onClick(View v) {
     * alertDialogBuilder.dismiss();
     * }
     * });
     * if (!title.isEmpty())
     * text_title_error_dialog.setText(title);
     * <p>
     * if (!((Activity) context).isFinishing()) {
     * if (context != null)
     * try {
     * alertDialogBuilder.show();
     * } catch (WindowManager.BadTokenException e) {
     * //use a log message
     * }
     * }
     * <p>
     * }
     */
    public static void showTwoActionDialog(Activity context, String title, String error, String actionName, final InstallCallback callback) {
        LayoutInflater inflater = context.getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.dialog_two_action_layout, null);
        final AlertDialog alertDialogBuilder = new AlertDialog.Builder(context).create();
        alertDialogBuilder.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        alertDialogBuilder.setView(dialoglayout);
        TextView text_title_error_dialog = dialoglayout.findViewById(R.id.text_title_error_dialog);
        TextView text_body_error_dialog = dialoglayout.findViewById(R.id.text_body_error_dialog);
        TextView close_dialog = dialoglayout.findViewById(R.id.close_dialog);
        TextView accept_dialog = dialoglayout.findViewById(R.id.accept_dialog);

        RadioGroup rg = (RadioGroup) dialoglayout.findViewById(R.id.radioGroup);
        TextView textselecttype = dialoglayout.findViewById(R.id.textselecttype);
        final String[] select = {""};
        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radioCash:
                        select[0] = RootManager.PaymentType_cash_Hand;
                        // do operations specific to this selection
                        break;
                    case R.id.radioGlobal:
                        // do operations specific to this selection
                        select[0] = RootManager.PaymentType_Global_Hand;
                        break;

                }
            }
        });


        accept_dialog.setText(actionName);
        accept_dialog.setTextColor(context.getResources().getColor(R.color.colorPrimary));
        FontManager.applyFont(context, dialoglayout);
        accept_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textselecttype.setError(null);
                textselecttype.clearFocus();
                if (!TextUtils.isEmpty(select[0])) {
                    callback.onStatusDone(select[0]);
                    alertDialogBuilder.dismiss();
                } else {
                    textselecttype.requestFocus();
                    textselecttype.setError(context.getResources().getString(R.string.required_field));
                }
            }
        });

        close_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogBuilder.dismiss();
            }
        });

        if (!title.isEmpty())
            text_title_error_dialog.setText(title);
        text_body_error_dialog.setText(error);
        if (!((Activity) context).isFinishing()) {
            if (context != null)
                try {
                    alertDialogBuilder.show();
                } catch (WindowManager.BadTokenException e) {
                    //use a log message
                }
        }

    }
    public static void showNullLocationDialog(Activity context, String title, String error, String actionName, final InstallCallback callback) {
        LayoutInflater inflater = context.getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.dialog_two_action_layout, null);
        final AlertDialog alertDialogBuilder = new AlertDialog.Builder(context).create();
        alertDialogBuilder.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        alertDialogBuilder.setView(dialoglayout);
        TextView text_title_error_dialog = dialoglayout.findViewById(R.id.text_title_error_dialog);
        TextView text_body_error_dialog = dialoglayout.findViewById(R.id.text_body_error_dialog);
        TextView close_dialog = dialoglayout.findViewById(R.id.close_dialog);
        TextView accept_dialog = dialoglayout.findViewById(R.id.accept_dialog);

        RadioGroup rg = (RadioGroup) dialoglayout.findViewById(R.id.radioGroup);
        TextView textselecttype = dialoglayout.findViewById(R.id.textselecttype);
        rg.setVisibility(View.GONE);
        textselecttype.setVisibility(View.GONE);

        accept_dialog.setText(actionName);
        accept_dialog.setTextColor(context.getResources().getColor(R.color.colorPrimary));
        FontManager.applyFont(context, dialoglayout);
        accept_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onStatusDone("retry");
                alertDialogBuilder.dismiss();
            }
        });

        close_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogBuilder.dismiss();
            }
        });

        if (!title.isEmpty())
            text_title_error_dialog.setText(title);
        text_body_error_dialog.setText(error);
        if (!((Activity) context).isFinishing()) {
            if (context != null)
                try {
                    alertDialogBuilder.setCancelable(false);
                    alertDialogBuilder.show();
                } catch (WindowManager.BadTokenException e) {
                    //use a log message
                }
        }

    }
    public static void showPermissionLocationBackGroundNotCancel(Activity context, String title, String error, final InstallCallback installCallback) {
        LayoutInflater inflater = context.getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.dialog_permission_location_layout, null);
        final AlertDialog alertDialogBuilder = new AlertDialog.Builder(context).create();
        alertDialogBuilder.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        alertDialogBuilder.setView(dialoglayout);
        TextView text_title_error_dialog = dialoglayout.findViewById(R.id.text_title_error_dialog);
        TextView text_body_error_dialog = dialoglayout.findViewById(R.id.text_body_error_dialog);
        TextView close_dialog = dialoglayout.findViewById(R.id.close_dialog);
        TextView btn_ok = dialoglayout.findViewById(R.id.btn_ok);

        CheckBox checkbox = dialoglayout.findViewById(R.id.checkbox);
        TextView text_privacy = dialoglayout.findViewById(R.id.text_privacy);

        FontManager.applyFont(context, dialoglayout);

        close_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogBuilder.dismiss();
                installCallback.onStatusDone("close");
            }
        });
        text_privacy.setOnClickListener(view -> {
            String url = "http://cn.sa/HatService/Privacy/index.html";
            String query = Uri.encode(url, "UTF-8");
            Intent browserIntent = new Intent(CATEGORY_BROWSABLE, Uri.parse(Uri.decode(query)));
            browserIntent.setAction(ACTION_VIEW);
            context.startActivity(browserIntent);
        });
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                text_privacy.setError(null);
                text_privacy.clearFocus();
                if(checkbox.isChecked()){
                alertDialogBuilder.dismiss();
                installCallback.onStatusDone("ok");
            }else {
                    text_privacy.setError("");
                    text_privacy.requestFocus();
                }
            }
        });

        if (!title.isEmpty())
            text_title_error_dialog.setText(title);
        text_body_error_dialog.setText(error);
        if (!((Activity) context).isFinishing()) {
            if (context != null)
                try {
                    alertDialogBuilder.setCancelable(true);
                    alertDialogBuilder.show();
                } catch (WindowManager.BadTokenException e) {
                    //use a log message
                }
        }

    }


}
