package com.app.hotfreshdelivery.Manager;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;

import java.util.Locale;

public class AppLanguage {

    public final static String ARABIC = "ar";
    public final static String PERSIAN = "fa";


    public static String getLanguage(Context context) {
        return AppPreferences.getString(context, "language",  "ar");
    }

    public static void saveLanguage(Context context, String language) {
        AppPreferences.saveString(context, "language", language);
    }

    private static void changeLang(Context context, String lang) {
        if (lang.equalsIgnoreCase(""))
            return;
        Locale myLocale = new Locale(lang);
        Locale.setDefault(myLocale);
        Configuration config = new Configuration();
        config.locale = myLocale;
        ((Activity) context).getBaseContext().getResources().updateConfiguration(config,
                ((Activity) context).getBaseContext().getResources().getDisplayMetrics());
//        updateTexts();
    }

    public static  void setContentLang(Activity context){
        String languageToLoad = AppLanguage.getLanguage(context); // your language
        Locale locale;
        switch (languageToLoad) {
            case "العربية":
                locale = new Locale("ar");
                break;
            case "English":
                locale = new Locale("en");
                break;
            case "ar":
                locale = new Locale("ar");
                break;
            case "en":
                locale = new Locale("en");
                break;
            default:
                locale = new Locale(languageToLoad);
                break;
        }
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        context.getBaseContext().getResources().updateConfiguration(config,
                context.getBaseContext().getResources().getDisplayMetrics());
    }
}
