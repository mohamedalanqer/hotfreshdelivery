package com.app.hotfreshdelivery.webService;


import com.app.hotfreshdelivery.Modules.User;
import com.app.hotfreshdelivery.webService.model.response.InstallResponse;
import com.app.hotfreshdelivery.webService.model.response.LoginResponse;
import com.app.hotfreshdelivery.webService.model.response.OrderResponse;
import com.app.hotfreshdelivery.webService.model.response.OrdersResponse;
import com.app.hotfreshdelivery.webService.model.response.RootResponse;
import com.app.hotfreshdelivery.webService.model.response.RoutesResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface RetrofitService {


    @GET("GetData.asmx/Settings")
    Call<InstallResponse> GetInstall();

    @FormUrlEncoded
    @POST("GetData.asmx/GetUserInfoByAccessToken")
    Call<LoginResponse> GetMyProfile(@Field("AccessToken") String AccessToken);

    //http://cn.sa/HatService\New/Delivery.asmx/Login 966512345679 وكلمة السر 123
    @POST("Delivery.asmx/Login")
    Call<LoginResponse> createLogin(@Body User user);


    @FormUrlEncoded
    @POST("Delivery.asmx/Login")
    Call<LoginResponse> createLogin(@Field("Mobile") String Mobile, @Field("PassWord") String PassWord,
                                    @Field("DeviceToken") String DeviceToken, @Field("TypeToken") String TypeToken);


    @FormUrlEncoded
    @POST("Delivery.asmx/GetDeliveryOrders")
    Call<OrdersResponse> GetMyOrdersList(@Field("AccessToken") String AccessToken,
                                         @Field("latpoint") String latpoint,
                                         @Field("longpoint") String longpoint);


    //http://cn.sa/HatService/New/Delivery.asmx/GetDeliveryOnWayOrders
    @FormUrlEncoded
    @POST("Delivery.asmx/GetDeliveryOnWayOrders")
    Call<OrdersResponse> GetOnWayOrdersList(@Field("AccessToken") String AccessToken);


    @FormUrlEncoded
    @POST("Delivery.asmx/GetDeliveryReceviedOrders")
    Call<OrdersResponse> GetDeliveryReceviedOrdersList(@Field("AccessToken") String AccessToken);


    @GET("GetData.asmx/OrderDetials")
    Call<OrderResponse> ShowOrdersById(@Query("BillCode") String BillCode);


    //http://cn.sa/HatService/New/Delivery.asmx/ChangeOrderStatus //0 on way  1 Recevied  ;
    @FormUrlEncoded
    @POST("Delivery.asmx/ChangeOrderStatus")
    Call<RootResponse> ChangeOrderStatus(@Field("AccessToken") String AccessToken, @Field("BillCode") String BillCode, @Field("StatusType") int StatusType, @Field("PaymentType") String PaymentType);


    @POST("auth/logout")
    Call<LoginResponse> logout();


    @GET("https://maps.googleapis.com/maps/api/directions/json")
    Call<RoutesResponse> GetDurations(@Query("key") String id,
                                      @Query("sensor") boolean sensor,
                                      @Query("language") String language,
                                      @Query("origin") String origin,
                                      @Query("destination") String destination,
                                      @Query("mode") String mode);


    @GET("https://maps.googleapis.com/maps/api/directions/json")
    Call<RoutesResponse> GetDurations(@Query("key") String id,
                                      @Query("sensor") boolean sensor,
                                      @Query("language") String language,
                                      @Query("origin") String origin,
                                      @Query("destination") String destination,
                                      @Query("waypoints") String waypoints,
                                      @Query("mode") String mode);
}
