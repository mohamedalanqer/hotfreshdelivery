package com.app.hotfreshdelivery.webService.model.response;

import com.app.hotfreshdelivery.Modules.User;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginResponse extends RootResponse {
    @SerializedName("DeliveryClass")
    @Expose
    public User user ;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
