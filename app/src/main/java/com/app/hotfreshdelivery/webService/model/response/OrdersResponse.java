package com.app.hotfreshdelivery.webService.model.response;

import com.app.hotfreshdelivery.Modules.Order;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OrdersResponse extends RootResponse {
    @SerializedName("DeliveryOrdersClass")
    @Expose
    public List<Order> orders ;

    public OrdersResponse() {
    }

    public OrdersResponse(List<Order> orders) {
        this.orders = orders;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }
}
