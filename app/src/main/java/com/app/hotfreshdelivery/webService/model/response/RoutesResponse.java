package com.app.hotfreshdelivery.webService.model.response;

import com.app.hotfreshdelivery.Modules.Map.Routes;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RoutesResponse {
    @SerializedName("routes")
    @Expose
    public List<Routes> routes;

    public List<Routes> getRoutes() {
        return routes;
    }

    public void setRoutes(List<Routes> routes) {
        this.routes = routes;
    }
}
