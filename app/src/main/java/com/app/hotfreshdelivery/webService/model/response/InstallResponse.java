package com.app.hotfreshdelivery.webService.model.response;


import com.app.hotfreshdelivery.Modules.Install.Branch;
import com.app.hotfreshdelivery.Modules.Install.Definition;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class InstallResponse extends RootResponse {
    @SerializedName("Branchs")
    @Expose
    public List<Branch> branches ;

    @SerializedName("Definitions")
    @Expose
    public  Definition Definition ;

    public InstallResponse() {
    }

    public List<Branch> getBranches() {
        return branches;
    }

    public void setBranches(List<Branch> branches) {
        this.branches = branches;
    }

    public  Definition getDefinition() {
        return Definition;
    }

    public void setDefinition( Definition definition) {
        Definition = definition;
    }

}
